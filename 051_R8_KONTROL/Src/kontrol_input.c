#include "kontrol_input.h"

Input_Union input_Stat;

uint8_t pull_up_input_read(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin) // sürekli +3v3 de ise giriş olduğunda 0 a çekiliyorsa  // TERS
{
  uint8_t result = 0 ;
   if(HAL_GPIO_ReadPin(GPIOx,GPIO_Pin)== GPIO_PIN_SET)
   {
     result = 0;
   }
   if(HAL_GPIO_ReadPin(GPIOx,GPIO_Pin)== GPIO_PIN_RESET)
   {
     result = 1;
   }
   return result;   
}
uint8_t pull_down_input_read(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin) // sürekli 0 da ise giriş olduğunda 1 e çekiliyorsa // NORMAL
{
  uint8_t result = 0 ;
   if(HAL_GPIO_ReadPin(GPIOx,GPIO_Pin)== GPIO_PIN_SET)
   {
     result = 1;
   }
   if(HAL_GPIO_ReadPin(GPIOx,GPIO_Pin)== GPIO_PIN_RESET)
   {
     result = 0;
   }	
   return result;
}
void input_durumu_guncelle()
{
 //input_Stat.bits.ACIL_STOP              = pull_up_input_read(ACIL_STOP_PORT             ,ACIL_STOP_PIN             );
 //input_Stat.bits.JY_YUKARI              = pull_up_input_read(JY_YUKARI_PORT             ,JY_YUKARI_PIN             );
 //input_Stat.bits.JY_ASAGI               = pull_up_input_read(JY_ASAGI_PORT              ,JY_ASAGI_PIN              );
 //input_Stat.bits.JY_SAG                 = pull_up_input_read(JY_SAG_PORT                ,JY_SAG_PIN                );
 //input_Stat.bits.JY_SOL                 = pull_up_input_read(JY_SOL_PORT                ,JY_SOL_PIN                );
 input_Stat.bits.SS_TANBUR_VAR          = pull_up_input_read(SS_TANBUR_VAR_PORT         ,SS_TANBUR_VAR_PIN         );
 input_Stat.bits.SS_VINC_SAG            = pull_up_input_read(SS_VINC_SAG_PORT           ,SS_VINC_SAG_PIN           );
 input_Stat.bits.SS_VINC_ORTA           = pull_up_input_read(SS_VINC_ORTA_PORT          ,SS_VINC_ORTA_PIN          );
 input_Stat.bits.SS_VINC_SOL            = pull_up_input_read(SS_VINC_SOL_PORT           ,SS_VINC_SOL_PIN           );
 input_Stat.bits.SS_VINC_YUKARI         = pull_up_input_read(SS_VINC_YUKARI_PORT        ,SS_VINC_YUKARI_PIN        );
 input_Stat.bits.SS_VINC_ASAGI          = pull_up_input_read(SS_VINC_ASAGI_PORT         ,SS_VINC_ASAGI_PIN         );
 input_Stat.bits.HOME_SWITCH            = pull_up_input_read(HOME_SWITCH_PORT           ,HOME_SWITCH_PIN           );
 input_Stat.bits.END_SWITCH             = pull_up_input_read(END_SWITCH_PORT            ,END_SWITCH_PIN            );

 input_Stat.bits.INPUT14                = pull_up_input_read(INPUT14_PORT               ,INPUT14_PIN               );
 input_Stat.bits.INPUT15                = pull_up_input_read(INPUT15_PORT               ,INPUT15_PIN               );

}