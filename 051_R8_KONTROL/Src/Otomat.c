#include "Otomat.h"

uint32_t tx_counter = 0 ;
uint8_t TX_usage_buffer[30] = {0};
uint8_t TX_usage_zaman_buffer[z_BUF_SIZE] = {SYN1,SYN2,0,0,0,0,0,0,0,0,0,0,SYN3};


void Kablosuz_Veri_Gonder(uint8_t func_id,uint8_t data_1,uint8_t data_2)
{
  TX_usage_buffer[SYN1_ADRES] = SYN1;
  TX_usage_buffer[SYN2_ADRES] = SYN2;
  TX_usage_buffer[FUNCTION_ID_ADRES] = func_id;
  TX_usage_buffer[DATA_1_ADRES] = data_1;
  TX_usage_buffer[DATA_2_ADRES] = data_2;
  TX_usage_buffer[SYN3_ADRES] = SYN3;
  TX_usage_buffer_sayisi = BUF_SIZE;
  pRadioDriver_rx->SM_State = SM_STATE_SEND_DATA;   // rx send yapildi 
  TIMERS[Kablosuz_Gonderme_Gecikmesi] = 65 ;
  tx_counter++;
}

void Kablosuz_Zaman_Verisi_Aktar(tambur_time t_var,uint8_t tambur_no)
{
  TX_usage_buffer[z_SYN1_ADRES] = SYN1;
  TX_usage_buffer[z_SYN2_ADRES] = SYN2;
  TX_usage_buffer[z_FUNCTION_ID_ADRES] = (uint8_t) TAMBUR_TUM_DEGERLER;
  TX_usage_buffer[z_TAMBUR_NO_ADRES] = tambur_no;
  TX_usage_buffer[z_IST_7_ADRES] = t_var.i_7_time;
  TX_usage_buffer[z_IST_6_ADRES] = t_var.i_6_time;
  TX_usage_buffer[z_IST_5_ADRES] = t_var.i_5_time;
  TX_usage_buffer[z_IST_4_ADRES] = t_var.i_4_time;
  TX_usage_buffer[z_IST_3_ADRES] = t_var.i_3_time;
  TX_usage_buffer[z_IST_2_ADRES] = t_var.i_2_time;
  TX_usage_buffer[z_IST_OWN]     = t_var.first_time;
  TX_usage_buffer[z_FLAG_ADRES]  = t_var.t_flag.flag;
  TX_usage_buffer[z_SYN3_ADRES] = SYN3;
  TX_usage_buffer_sayisi = z_BUF_SIZE;
  
  pRadioDriver_rx->SM_State = SM_STATE_SEND_DATA;   // rx send yapildi 
  TIMERS[Kablosuz_Gonderme_Gecikmesi] = 65 ;
}