/**
******************************************************************************
* @file    spirit1_appli.c
* @author  CLAB
* @version V1.2.0
* @date    10-Sep-2016
* @brief   user file to configure Spirit1 transceiver.
*         
@verbatim
===============================================================================
##### How to use this driver #####
===============================================================================
[..]
This file is generated automatically by STM32CubeMX and eventually modified 
by the user

@endverbatim
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/ 

/* Includes ------------------------------------------------------------------*/
#include "cube_hal.h"
#include "spirit1_appli.h"
#include "MCU_Interface.h" 
#include "SPIRIT1_Util.h"

/** @addtogroup USER
* @{
*/
//extern UART_HandleTypeDef huart1;
uint8_t uart_deneme[6]="DATA\n\r";
extern uint32_t SUB1_TIME_FLAG; 
uint8_t uart_bagli ; 
extern uint8_t master_id;
extern uint8_t slave_id;

/** @defgroup SPIRIT1_APPLI
* @brief User file to configure spirit1 transceiver for desired frequency and 
* @feature.
* @{
*/

/* Private typedef -----------------------------------------------------------*/
extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
/**
* @brief RadioDriver_t structure fitting
*/
//////////////////SPI 1 ////////////////////////////////
GPIO_TypeDef* RADIO_RX_GPIO_PORT[RADIO_GPIO_NUMBER] = {
  0,
  0,
  0,
  GPIOB, // GPIO 3
  GPIOC //RADIO_GPIO_SDN_PORT
};

uint16_t RADIO_RX_GPIO_PIN[RADIO_GPIO_NUMBER] = {
  0,
  0,
  0,
  GPIO_PIN_0,//RADIO_GPIO_3_PIN,
  GPIO_PIN_4//RADIO_GPIO_SDN_PIN   
};
uint8_t RADIO_GPIO_RX_IRQn[5-1] = {
  0,
  0,
  0,
  EXTI0_1_IRQn  
};



//////////////////SPI 3 ////////////////////////////////
GPIO_TypeDef* RADIO_TX_GPIO_PORT[RADIO_GPIO_NUMBER] = {
  0,
  0,
  0,
  GPIOC, // GPIO 3
  GPIOC //RADIO_GPIO_SDN_PORT
};

uint16_t RADIO_TX_GPIO_PIN[RADIO_GPIO_NUMBER] = {
  0,
  0,
  0,
  GPIO_PIN_7,//RADIO_GPIO_3_PIN,
  GPIO_PIN_6//RADIO_GPIO_SDN_PIN   
};
uint8_t RADIO_GPIO_TX_IRQn[5-1] = {
  0,
  0,
  0,
  EXTI4_15_IRQn
};

SPI_Handle spirit_spi_tx = 
{
 .CS_Port = GPIOB,
 .CS_Pin = GPIO_PIN_12,
 .pSpiHandle = &hspi2,
 .MY_ADRES = 0x96, 
 .DEST_ADRES = 0x90,   /// EX CHANGE  0x22 or�j�nal hal�
 .g_xStatus = 0,
};

SPI_Handle spirit_spi_rx = 
{
 .CS_Port = GPIOA ,
 .CS_Pin = GPIO_PIN_4,
 .pSpiHandle = &hspi1,
 .MY_ADRES = 0x50,
 .DEST_ADRES = 0x30,
 .g_xStatus = 0,
};


RadioDriver_t spirit_rx =
{
  .Init = Spirit1InterfaceInit, 
  .GpioIrq = Spirit1GpioIrqInit,
  .RadioInit = Spirit1RadioInit,
  .SetRadioPower = Spirit1SetPower,
  .PacketConfig = Spirit1PacketConfig,
  .SetPayloadLen = Spirit1SetPayloadlength,
  .SetDestinationAddress = Spirit1SetDestinationAddress,
  .EnableTxIrq = Spirit1EnableTxIrq,
  .EnableRxIrq = Spirit1EnableRxIrq,
  .DisableIrq = Spirit1DisableIrq,
  .SetRxTimeout = Spirit1SetRxTimeout,
  .EnableSQI = Spirit1EnableSQI,
  .SetRssiThreshold = Spirit1SetRssiTH,
  .ClearIrqStatus = Spirit1ClearIRQ,
  .StartRx = Spirit1StartRx,
  .StartTx = Spirit1StartTx,
  .GetRxPacket = Spirit1GetRxPacket,
  .spirit_spi = &spirit_spi_rx , 
};
RadioDriver_t spirit_tx =
{
  .Init = Spirit1InterfaceInit, 
  .GpioIrq = Spirit1GpioIrqInit,
  .RadioInit = Spirit1RadioInit,
  .SetRadioPower = Spirit1SetPower,
  .PacketConfig = Spirit1PacketConfig,
  .SetPayloadLen = Spirit1SetPayloadlength,
  .SetDestinationAddress = Spirit1SetDestinationAddress,
  .EnableTxIrq = Spirit1EnableTxIrq,
  .EnableRxIrq = Spirit1EnableRxIrq,
  .DisableIrq = Spirit1DisableIrq,
  .SetRxTimeout = Spirit1SetRxTimeout,
  .EnableSQI = Spirit1EnableSQI,
  .SetRssiThreshold = Spirit1SetRssiTH,
  .ClearIrqStatus = Spirit1ClearIRQ,
  .StartRx = Spirit1StartRx,
  .StartTx = Spirit1StartTx,
  .GetRxPacket = Spirit1GetRxPacket,
  .spirit_spi = &spirit_spi_tx , 
};

/**
* @brief MCULowPowerMode_t structure fitting
*/
MCULowPowerMode_t MCU_LPM_cb =
{
  .McuStopMode = MCU_Enter_StopMode,
  .McuStandbyMode = MCU_Enter_StandbyMode,
  .McuSleepMode = MCU_Enter_SleepMode
}; 

/**
* @brief RadioLowPowerMode_t structure fitting
*/
RadioLowPowerMode_t Radio_LPM_cb =
{
  .RadioShutDown = RadioPowerOFF,
  .RadioStandBy = RadioStandBy,
  .RadioSleep = RadioSleep,
  .RadioPowerON = RadioPowerON
};

/**
* @brief GPIO structure fitting
*/
SGpioInit xGpioIRQ={
  SPIRIT_GPIO_IRQ,
  SPIRIT_GPIO_MODE_DIGITAL_OUTPUT_LP,
  SPIRIT_GPIO_DIG_OUT_IRQ
};

/**
* @brief Radio structure fitting
*/
SRadioInit xRadioInit = {
  XTAL_OFFSET_PPM,
  BASE_FREQUENCY,
  CHANNEL_SPACE,
  CHANNEL_NUMBER,
  MODULATION_SELECT,
  DATARATE,
  FREQ_DEVIATION,
  BANDWIDTH
};


#if defined(USE_STack_PROTOCOL)
/**
* @brief Packet Basic structure fitting
*/
PktStackInit xStackInit={
  PREAMBLE_LENGTH,
  SYNC_LENGTH,
  SYNC_WORD,
  LENGTH_TYPE,
  LENGTH_WIDTH,
  CRC_MODE,
  CONTROL_LENGTH,
  EN_FEC,
  EN_WHITENING
};

/* LLP structure fitting */
PktStackLlpInit xStackLLPInit ={
  EN_AUTOACK,
  EN_PIGGYBACKING,
  MAX_RETRANSMISSIONS
};

/**
* @brief Address structure fitting
*/
PktStackAddressesInit xAddressInit={
  EN_FILT_MY_ADDRESS,
  MY_ADDRESS,
  EN_FILT_MULTICAST_ADDRESS,
  MULTICAST_ADDRESS,
  EN_FILT_BROADCAST_ADDRESS,
  BROADCAST_ADDRESS
};

#elif defined(USE_BASIC_PROTOCOL)

/**
* @brief Packet Basic structure fitting
*/
PktBasicInit xBasicInit={
  PREAMBLE_LENGTH,
  SYNC_LENGTH,
  SYNC_WORD,
  LENGTH_TYPE,
  LENGTH_WIDTH,
  CRC_MODE,
  CONTROL_LENGTH,
  EN_ADDRESS,
  EN_FEC,
  EN_WHITENING
};


/**
* @brief Address structure fitting
*/
PktBasicAddressesInit xAddressInit={
  EN_FILT_MY_ADDRESS,
  0,//MY_ADDRESS --dont use
  EN_FILT_MULTICAST_ADDRESS,
  MULTICAST_ADDRESS,
  EN_FILT_BROADCAST_ADDRESS,
  BROADCAST_ADDRESS
};

#endif

#ifdef CSMA_ENABLE
  /**
  * @brief CSMA structure fitting
  */
  CsmaInit xCsmaInit={
    PERSISTENT_MODE_EN, /* CCA may optionally be persistent, i.e., rather than 
    entering backoff when the channel is found busy, CCA continues until the 
    channel becomes idle or until the MCU stops it.
    The thinking behind using this option is to give the MCU the possibility of 
    managing the CCA by itself, for instance, with the allocation of a 
    transmission timer: this timer would start when MCU finishes sending out 
    data to be transmitted, and would end when MCU expects that its transmission
    takes place, which would occur after a period of CCA.
    The choice of making CCA persistent should come from trading off 
    transmission latency,under the direct control of the MCU, and power 
    consumption, which would be greater due to a busy wait in reception mode.
                          */
    CS_PERIOD,
    CS_TIMEOUT,
    MAX_NB,
    BU_COUNTER_SEED,
    CU_PRESCALER
  };
#endif 
  
/* Private define ------------------------------------------------------------*/
#define TIME_UP                                         0x01

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
RadioDriver_t *pRadioDriver_rx;
RadioDriver_t *pRadioDriver_tx;
MCULowPowerMode_t *pMCU_LPM_Comm;
RadioLowPowerMode_t  *pRadio_LPM_Comm;


/*Flags declarations*/

/*IRQ status struct declaration*/
//SpiritIrqs xIrqStatus;
__IO uint32_t KEYStatusData = 0x00;
//static AppliFrame_t xTxFrame, xRxFrame;
//uint8_t DestinationAddr;
uint8_t TxFrameBuff[MAX_BUFFER_LEN] = {0x00};
//uint16_t exitCounter = 0;
uint16_t txCounter = 0;
//uint16_t wakeupCounter = 0;
//uint16_t dataSendCounter = 0x00;

/* Private function prototypes -----------------------------------------------*/

//void HAL_Spirit1_Init(void);
//void Enter_LP_mode(void);
//void Exit_LP_mode(void);
//void MCU_Enter_StopMode(void);
//void MCU_Enter_StandbyMode(void);
//void MCU_Enter_SleepMode(void);
//void RadioPowerON(void);
//void RadioPowerOFF(void);
//void RadioStandBy(void);
//void RadioSleep(void);
//void AppliSendBuff(AppliFrame_t *xTxFrame, uint8_t cTxlen);
//void AppliReceiveBuff(uint8_t *RxFrameBuff, uint8_t cRxlen);
//void P2P_Init(void);
//void STackProtocolInit(void);
//void BasicProtocolInit(void);
//void P2PInterruptHandler(void);
//void Set_KeyStatus(FlagStatus val);
//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
//void HAL_SYSTICK_Callback(void);

/* Private functions ---------------------------------------------------------*/

/** @defgroup SPIRIT1_APPLI_Private_Functions
* @{
*/

/**
* @brief  Initializes RF Transceiver's HAL.
* @param  None
* @retval None.
*/
void HAL_Spirit1_Init(void)
{
  for(int i = 0 ; i< 5 ; i++)
  {
   spirit_rx.spirit_spi->RADIO_GPIO_PORT[i] =  RADIO_RX_GPIO_PORT[i];
   spirit_tx.spirit_spi->RADIO_GPIO_PORT[i] =  RADIO_TX_GPIO_PORT[i];
   
   spirit_rx.spirit_spi->RADIO_GPIO_PIN[i] =  RADIO_RX_GPIO_PIN[i];
   spirit_tx.spirit_spi->RADIO_GPIO_PIN[i] =  RADIO_TX_GPIO_PIN[i];
  }
  for(int i = 0 ; i< 4 ; i++)
  { 
   spirit_rx.spirit_spi->RADIO_GPIO_IRQn[i] =  RADIO_GPIO_RX_IRQn[i];
   spirit_tx.spirit_spi->RADIO_GPIO_IRQn[i] =  RADIO_GPIO_TX_IRQn[i];
  }
  
  //pRadioDriver_tx = &spirit_tx;
  //pRadioDriver_tx->Init(pRadioDriver_tx->spirit_spi);
  
  pRadioDriver_rx = &spirit_rx;
  pRadioDriver_rx->Init(pRadioDriver_rx->spirit_spi); 
  

  
  
}

//SM_State_t SM_State = SM_STATE_START_RX;/* The actual state running */

/**
* @brief  SPIRIT1 P2P Process State machine
* @param  uint8_t *pTxBuff = Pointer to aTransmitBuffer 
*         uint8_t cTxlen = length of aTransmitBuffer 
*         uint8_t* pRxBuff = Pointer to aReceiveBuffer
*         uint8_t cRxlen= length of aReceiveBuffer
* @retval None.
*/
uint8_t asdasdasd = 0 ;
void P2P_Process(RadioDriver_t *pRadioDriver,uint8_t *pTxBuff, uint8_t cTxlen, uint8_t* pRxBuff, uint8_t cRxlen)
{
  switch(pRadioDriver->SM_State)
  {
  case SM_STATE_START_RX:
    {      
      AppliReceiveBuff(pRadioDriver,pRxBuff, cRxlen);
      /* wait for data received or timeout period occured */
      pRadioDriver->SM_State = SM_STATE_WAIT_FOR_RX_DONE;    
    }
    break;
    
  case SM_STATE_WAIT_FOR_RX_DONE:
    if((RESET != pRadioDriver->xRxDoneFlag)||(RESET != pRadioDriver->rx_timeout)||(SET != pRadioDriver->exitTime))
    {
      if((pRadioDriver->rx_timeout==SET)||(pRadioDriver->exitTime==RESET))
      {
        pRadioDriver->rx_timeout = RESET;
        //HAL_GPIO_TogglePin(GPIOE,GPIO_PIN_0);
        //BSP_LED_Toggle(LED2);
        pRadioDriver->SM_State = SM_STATE_START_RX;
      }
      else if(pRadioDriver->xRxDoneFlag) 
      {
        pRadioDriver->xRxDoneFlag=RESET;
        pRadioDriver->SM_State = SM_STATE_DATA_RECEIVED;
      }
    }
    break;
    
  case SM_STATE_DATA_RECEIVED:
    {
      pRadioDriver->GetRxPacket(pRadioDriver->spirit_spi,pRxBuff,&cRxlen); 
      /*rRSSIValue = Spirit1GetRssiTH();*/
      pRadioDriver->xRxFrame.Cmd = pRxBuff[0];
      pRadioDriver->xRxFrame.CmdLen = pRxBuff[1];
      pRadioDriver->xRxFrame.Cmdtag = pRxBuff[2];
      pRadioDriver->xRxFrame.CmdType = pRxBuff[3];
      pRadioDriver->xRxFrame.DataLen = pRxBuff[4];
      if(pRadioDriver->xRxFrame.Cmd == PROCESS)
      {
        pRadioDriver->SM_State = SM_STATE_DATA_PROCESS; 
      } 
      if(pRadioDriver->xRxFrame.Cmd == ACK_OK)
      {
        pRadioDriver->SM_State = SM_STATE_ACK_RECEIVED;
      }
    }
    break;
    
  case SM_STATE_SEND_ACK:
    {
      pRadioDriver->xTxFrame.Cmd = ACK_OK;
      pRadioDriver->xTxFrame.CmdLen = 0x01;
      pRadioDriver->xTxFrame.Cmdtag = pRadioDriver->xRxFrame.Cmdtag;
      pRadioDriver->xTxFrame.CmdType = APPLI_CMD;
      pRadioDriver->xTxFrame.DataBuff = pTxBuff;
      pRadioDriver->xTxFrame.DataLen = cTxlen;
            
       
      AppliSendBuff(pRadioDriver,&pRadioDriver->xTxFrame);   
      pRadioDriver->SM_State = SM_STATE_WAIT_FOR_TX_DONE;
    }
    break;   
      
  case SM_STATE_SEND_DATA:
    {
      pRadioDriver->xTxFrame.Cmd = PROCESS;
      pRadioDriver->xTxFrame.CmdLen = 0x01;
      pRadioDriver->xTxFrame.Cmdtag = txCounter++;
      pRadioDriver->xTxFrame.CmdType = APPLI_CMD;
      pRadioDriver->xTxFrame.DataBuff = pTxBuff;
      pRadioDriver->xTxFrame.DataLen = cTxlen;  
      AppliSendBuff(pRadioDriver,&pRadioDriver->xTxFrame);
      pRadioDriver->SM_State = SM_STATE_WAIT_FOR_TX_DONE;
    }
    break;
    
  case SM_STATE_WAIT_FOR_TX_DONE:
  /* wait for TX done */    
    if(pRadioDriver->xTxDoneFlag)
    {
      pRadioDriver->xTxDoneFlag = RESET;
      
      if(pRadioDriver->xTxFrame.Cmd == PROCESS)
      {
        pRadioDriver->SM_State = SM_STATE_START_RX;
      }
      else if(pRadioDriver->xTxFrame.Cmd == ACK_OK)
      {
        pRadioDriver->SM_State = SM_STATE_IDLE;  
      }
    }
    break;
    
  case SM_STATE_ACK_RECEIVED:
    //for(; ledToggleCtr<5; ledToggleCtr++)
    //{
     
      //HAL_Delay(DELAY_RX_LED_TOGGLE);
    //}
    pRadioDriver->SM_State = SM_STATE_IDLE;   
    break;
    
  case SM_STATE_DATA_PROCESS:
      //RadioShieldLedOn(RADIO_SHIELD_LED);
      pRadioDriver->RF_Data_Geldi = 1 ;      
      memcpy(pRadioDriver->RF_usage_buffer,&pRxBuff[5],pRxBuff[4]); // 5.indexten itibaren yaz. data length ise 4.index de�eridir.
      pRadioDriver->RF_usage_buffer_Sayisi = pRadioDriver->xRxFrame.DataLen;
      uint8_t dest_addr;
      dest_addr = SpiritPktCommonGetReceivedDestAddress(pRadioDriver->spirit_spi);
      
      if ((dest_addr == MULTICAST_ADDRESS) || (dest_addr == BROADCAST_ADDRESS))
      {
        /* in that case do not send ACK to avoid RF collisions between several RF ACK messages */
       
        pRadioDriver->SM_State = SM_STATE_IDLE;   
      }
      else
      {
        pRadioDriver->SM_State = SM_STATE_IDLE;  
      }
    break;
    
  case SM_STATE_IDLE:
    //RadioShieldLedOff(RADIO_SHIELD_LED);
    //BSP_LED_Off(LED2); 
    pRadioDriver->SM_State = SM_STATE_START_RX;
    
#if defined(USE_LOW_POWER_MODE)
    Enter_LP_mode();
#endif     
    break;
  }
}

/**
* @brief  This function handles the point-to-point packet transmission
* @param  AppliFrame_t *xTxFrame = Pointer to AppliFrame_t structure 
*         uint8_t cTxlen = Length of aTransmitBuffer
* @retval None
*/
void AppliSendBuff(RadioDriver_t *pRadioDriver,AppliFrame_t *xTxFrame)
{
  uint8_t xIndex = 0;
  uint8_t trxLength = 0;
    
 SpiritPktBasicAddressesInit(pRadioDriver->spirit_spi,&xAddressInit);
 
   
  TxFrameBuff[0] = xTxFrame->Cmd;
  TxFrameBuff[1] = xTxFrame->CmdLen;
  TxFrameBuff[2] = xTxFrame->Cmdtag;
  TxFrameBuff[3] = xTxFrame->CmdType;
  TxFrameBuff[4] = xTxFrame->DataLen;
  
  for(; xIndex < xTxFrame->DataLen; xIndex++)
  {
    TxFrameBuff[xIndex+5] =  xTxFrame->DataBuff[xIndex];
  }
  
  
  trxLength = (xIndex+5);
  
  /* Spirit IRQs disable */
  pRadioDriver->DisableIrq(pRadioDriver->spirit_spi);
  /* Spirit IRQs enable */
  pRadioDriver->EnableTxIrq(pRadioDriver->spirit_spi);
  /* payload length config */
  pRadioDriver->SetPayloadLen(pRadioDriver->spirit_spi,trxLength);  
  /* rx timeout config */
  pRadioDriver->SetRxTimeout(pRadioDriver->spirit_spi,RECEIVE_TIMEOUT);
  /* IRQ registers blanking */
  pRadioDriver->ClearIrqStatus(pRadioDriver->spirit_spi);
  /* destination address */
  pRadioDriver->SetDestinationAddress(pRadioDriver->spirit_spi,pRadioDriver->spirit_spi->DEST_ADRES);
  /* send the TX command */
  pRadioDriver->StartTx(pRadioDriver->spirit_spi,TxFrameBuff, trxLength);
}


/**
* @brief  This function handles the point-to-point packet reception
* @param  uint8_t *RxFrameBuff = Pointer to ReceiveBuffer
*         uint8_t cRxlen = length of ReceiveBuffer
* @retval None
*/
void AppliReceiveBuff(RadioDriver_t *pRadioDriver,uint8_t *RxFrameBuff, uint8_t cRxlen)
{
  /*float rRSSIValue = 0;*/
  pRadioDriver->exitTime = SET;
  pRadioDriver->exitCounter = TIME_TO_EXIT_RX;

#ifdef USE_STack_PROTOCOL
  
#ifdef USE_STack_LLP
    /* LLP structure fitting */
  PktStackLlpInit xStackLLPInit=
  {
    .xAutoAck = S_ENABLE,                
    .xPiggybacking = S_ENABLE,              
    .xNMaxRetx = PKT_DISABLE_RETX
  }; 
#else
  /* LLP structure fitting */
  PktStackLlpInit xStackLLPInit=
  {
    .xAutoAck = S_DISABLE,                
    .xPiggybacking = S_DISABLE,              
    .xNMaxRetx = PKT_DISABLE_RETX
  }; 
#endif
  SpiritPktStackLlpInit(&xStackLLPInit);
  
  PktStackAddressesInit xAddressInit=
  {
    .xFilterOnMyAddress = S_ENABLE,
    .cMyAddress = slave_id,
    .xFilterOnMulticastAddress = S_DISABLE,
    .cMulticastAddress = MULTICAST_ADDRESS,
    .xFilterOnBroadcastAddress = S_ENABLE,
    .cBroadcastAddress = BROADCAST_ADDRESS
  };
  
  SpiritPktStackAddressesInit(&xAddressInit);
  
  if(EN_FILT_SOURCE_ADDRESS)
  {
    SpiritPktStackFilterOnSourceAddress(S_ENABLE);
    SpiritPktStackSetRxSourceMask(SOURCE_ADDR_MASK);
    SpiritPktStackSetSourceReferenceAddress(SOURCE_ADDR_REF);
    
  }
  else
  {
    SpiritPktStackFilterOnSourceAddress(S_DISABLE);    
  }
#endif

#ifdef USE_BASIC_PROTOCOL
  
    SpiritPktBasicAddressesInit(pRadioDriver->spirit_spi,&xAddressInit);

#endif  
  
  /* Spirit IRQs disable */
  pRadioDriver->DisableIrq(pRadioDriver->spirit_spi);
  /* Spirit IRQs enable */
  pRadioDriver->EnableRxIrq(pRadioDriver->spirit_spi);
  /* payload length config */
  pRadioDriver->SetPayloadLen(pRadioDriver->spirit_spi,PAYLOAD_LEN);
  /* rx timeout config */
  pRadioDriver->SetRxTimeout(pRadioDriver->spirit_spi,RECEIVE_TIMEOUT);
  /* destination address */
  pRadioDriver->SetDestinationAddress(pRadioDriver->spirit_spi,pRadioDriver->spirit_spi->DEST_ADRES);  
  /* IRQ registers blanking */
  pRadioDriver->ClearIrqStatus(pRadioDriver->spirit_spi); 
  /* RX command */
  pRadioDriver->StartRx(pRadioDriver->spirit_spi);      
}

/**
* @brief  This function initializes the protocol for point-to-point 
* communication
* @param  None
* @retval None
*/
void P2P_Init(RadioDriver_t *pRadioDriver)
{
 // DestinationAddr=master_id;
  
     /* Spirit IRQ config */
  pRadioDriver->GpioIrq(pRadioDriver->spirit_spi,&xGpioIRQ);
  
  /* Spirit Radio config */    
  pRadioDriver->RadioInit(pRadioDriver->spirit_spi,&xRadioInit);
  
  /* Spirit Radio set power */
  pRadioDriver->SetRadioPower(pRadioDriver->spirit_spi,POWER_INDEX, POWER_DBM);  
  
  /* Spirit Packet config */  
  pRadioDriver->PacketConfig(pRadioDriver->spirit_spi);
  
  pRadioDriver->EnableSQI(pRadioDriver->spirit_spi);
  
  pRadioDriver->SetRssiThreshold(pRadioDriver->spirit_spi,RSSI_THRESHOLD);
}

/**
* @brief  This function initializes the STack Packet handler of spirit1
* @param  None
* @retval None
*/
void STackProtocolInit(RadioDriver_t *pRadioDriver)
{ 
  
   PktStackInit xStackInit=
   {
     .xPreambleLength = PREAMBLE_LENGTH,
     .xSyncLength = SYNC_LENGTH,
     .lSyncWords = SYNC_WORD,
     .xFixVarLength = LENGTH_TYPE,
     .cPktLengthWidth = 8,
     .xCrcMode = CRC_MODE,
     .xControlLength = CONTROL_LENGTH,
     .xFec = EN_FEC,
     .xDataWhitening = EN_WHITENING
   };  
   
   /* Spirit Packet config */
   SpiritPktStackInit(pRadioDriver->spirit_spi,&xStackInit);
}  
                   
/*xDataWhitening;  *
* @brief  This function initializes the BASIC Packet handler of spirit1
* @param  None
* @retval None
*/
void BasicProtocolInit(SPI_Handle *spi_t)
{
  /* Spirit Packet config */
  SpiritPktBasicInit(spi_t,&xBasicInit);
}

/**
* @brief  This routine will put the radio and mcu in LPM
* @param  None
* @retval None
*/
void Enter_LP_mode(void)
{  
  pMCU_LPM_Comm = &MCU_LPM_cb;
  pRadio_LPM_Comm = &Radio_LPM_cb;
  
#if defined(MCU_SLEEP_MODE)&&defined(RF_SHUTDOWN) 
  {
    pRadio_LPM_Comm->RadioShutDown(); 
    pMCU_LPM_Comm->McuSleepMode();
  }
#elif defined(MCU_SLEEP_MODE)&&defined(RF_STANDBY) 
  {
    pRadio_LPM_Comm->RadioStandBy(); 
    pMCU_LPM_Comm->McuSleepMode();
  }
#elif defined(MCU_SLEEP_MODE)&&defined(RF_SLEEP) 
  {
    pRadio_LPM_Comm->RadioSleep();
    pMCU_LPM_Comm->McuSleepMode();
  }
#elif defined(MCU_STOP_MODE)&&defined(RF_SHUTDOWN) 
  {
    pRadio_LPM_Comm->RadioShutDown(); 
    pMCU_LPM_Comm->McuStopMode();
  }
#elif defined(MCU_STOP_MODE)&&defined(RF_STANDBY) 
  {
    pRadio_LPM_Comm->RadioStandBy(); 
    pMCU_LPM_Comm->McuStopMode();
  }
#elif defined(MCU_STOP_MODE)&&defined(RF_SLEEP) 
  {
    pRadio_LPM_Comm->RadioSleep();
    pMCU_LPM_Comm->McuStopMode();
  }
#else
  pMCU_LPM_Comm->McuSleepMode();
#endif
}

/**
* @brief  This routine wake-up the mcu and radio from LPM
* @param  None
* @retval None
*/
void Exit_LP_mode(SPI_Handle *spi_t)
{
  pRadio_LPM_Comm = &Radio_LPM_cb;      
  pRadio_LPM_Comm->RadioPowerON(spi_t);  
}

/**
* @brief  This routine puts the MCU in stop mode
* @param  None
* @retval None
*/
void MCU_Enter_StopMode(void)
{
  APPLI_EnterSTOPMode();
}

/**
* @brief  This routine puts the MCU in standby mode
* @param  None
* @retval None
*/
void MCU_Enter_StandbyMode(void)
{
  APPLI_EnterSTANDBYMode();
}

/**
* @brief  This routine puts the MCU in sleep mode
* @param  None
* @retval None
*/
void MCU_Enter_SleepMode(void)
{
  /*Suspend Tick increment to prevent wakeup by Systick interrupt. 
  Otherwise the Systick interrupt will wake up the device within 1ms (HAL time base)*/

  APPLI_EnterSLEEPMode();
}

/**
* @brief  This function will turn on the radio and waits till it enters the Ready state.
* @param  Param:None. 
* @retval None
*                       
*/
void RadioPowerON(SPI_Handle *spi_t)
{
  SpiritCmdStrobeReady(spi_t);   
  do{
    /* Delay for state transition */
    for(volatile uint8_t i=0; i!=0xFF; i++);
    
    /* Reads the MC_STATUS register */
    SpiritRefreshStatus(spi_t);
  }
  while(spi_t->g_xStatus.MC_STATE!=MC_STATE_READY);
}


/**
* @brief  This function will Shut Down the radio.
* @param  Param:None. 
* @retval None
*                       
*/
void RadioPowerOFF(SPI_Handle *spi_t)
{
  SpiritEnterShutdown(spi_t);
}


/**
* @brief  This function will put the radio in standby state.
* @param  None. 
* @retval None
*                       
*/
void RadioStandBy(SPI_Handle *spi_t)
{
  SpiritCmdStrobeStandby(spi_t);  
}

/**
* @brief  This function will put the radio in sleep state.
* @param  None. 
* @retval None
*                       
*/
void RadioSleep(SPI_Handle *spi_t)
{
  SpiritCmdStrobeSleep(spi_t); 
}

void Spirit1PacketConfig(SPI_Handle *spi_t)
{
#if defined(USE_STack_PROTOCOL)
  
  STackProtocolInit();
   
#elif defined(USE_BASIC_PROTOCOL)
  
  BasicProtocolInit(spi_t);
  
#endif
}
/**
* @brief  This routine updates the respective status for key press.
* @param  None
* @retval None
*/
void Set_KeyStatus(FlagStatus val)
{
  if(val==SET)
  {
    //SM_State = SM_STATE_SEND_DATA;
  }
}

/**
* @brief  This function handles External interrupt request. In this application it is used
*         to manage the Spirit IRQ configured to be notified on the Spirit GPIO_3.
* @param  None
* @retval None
*/
void P2PInterruptHandler(RadioDriver_t *pRadioDriver)
{
  SpiritIrqGetStatus(pRadioDriver->spirit_spi,&pRadioDriver->xIrqStatus);
  
  /* Check the SPIRIT TX_DATA_SENT IRQ flag */
  if(
     (pRadioDriver->xIrqStatus.IRQ_TX_DATA_SENT) 
       
#ifdef CSMA_ENABLE
       ||(pRadioDriver->xIrqStatus.IRQ_MAX_BO_CCA_REACH)
#endif
    )
  {
#ifdef CSMA_ENABLE
      SpiritCsma(pRadioDriver->spirit_spi,S_DISABLE);  
      SpiritRadioPersistenRx(pRadioDriver->spirit_spi,S_ENABLE);
      SpiritRadioCsBlanking(pRadioDriver->spirit_spi,S_ENABLE);
      
      if(pRadioDriver->xIrqStatus.IRQ_MAX_BO_CCA_REACH)
      {
        SpiritCmdStrobeSabort(pRadioDriver->spirit_spi);
      }

      SpiritQiSetRssiThresholddBm(pRadioDriver->spirit_spi,RSSI_THRESHOLD);
#endif

      pRadioDriver->xTxDoneFlag = SET;
  }
  
  /* Check the SPIRIT RX_DATA_READY IRQ flag */
  if((pRadioDriver->xIrqStatus.IRQ_RX_DATA_READY))
  {
    pRadioDriver->xRxDoneFlag = SET;   
  }
  
  /* Restart receive after receive timeout*/
  if (pRadioDriver->xIrqStatus.IRQ_RX_TIMEOUT)
  {
    pRadioDriver->rx_timeout = SET; 
    SpiritCmdStrobeRx(pRadioDriver->spirit_spi);
    SUB1_TIME_FLAG = 0 ;
  }
  
  /* Check the SPIRIT RX_DATA_DISC IRQ flag */
  if(pRadioDriver->xIrqStatus.IRQ_RX_DATA_DISC)
  {      
    /* RX command - to ensure the device will be ready for the next reception */
    SpiritCmdStrobeRx(pRadioDriver->spirit_spi);
  }  
}

/**
  * @brief  SYSTICK callback.
  * @param  None
  * @retval None
  */
void HAL_SYSTICK_Callback(void)
{

  TimeOutControl(pRadioDriver_rx);
  TimeOutControl(pRadioDriver_tx);
  
#if defined(RF_STANDBY)
  /*Check if Push Button pressed for wakeup or to send data*/
  if(PushButtonStatusWakeup)
  {
    /*Decreament the counter to check when 5 seconds has been elapsed*/  
    wakeupCounter--;
    
    /*5seconds has been elapsed*/
    if(wakeupCounter<=TIME_UP)
    {
      /*Perform wakeup opeartion*/      
      Exit_LP_mode();
      PushButtonStatusWakeup = RESET;
      PushButtonStatusData = SET;
    }
  }
  else if(PushButtonStatusData)
  {
    dataSendCounter--;
    if(dataSendCounter<=TIME_UP)
    {
      Set_KeyStatus(SET);
      PushButtonStatusWakeup = RESET;
      PushButtonStatusData = RESET;
    }
  }
#endif
}
/**
* @}
*/

/**
  * @brief GPIO EXTI callback
  * @param uint16_t GPIO_Pin
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  
#if defined(RF_STANDBY)/*if spirit1 is in standby*/
  #if defined(MCU_STOP_MODE)/*if MCU is in stop mode*/        
  
  /* Clear Wake Up Flag */
  __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
  
  /* Configures system clock after wake-up from STOP: enable HSE, PLL and select
  PLL as system clock source (HSE and PLL are disabled in STOP mode) */
  SystemClockConfig_ExitSTOPMode(); 
  #endif
  #if defined(MCU_SLEEP_MODE) 
  /* Resume Tick interrupt if disabled prior to sleep mode entry*/
  HAL_ResumeTick();
  #endif 
  
  /* Initialize LEDs*/
  //RadioShieldLedInit(RADIO_SHIELD_LED);
  //BSP_LED_Init(LED2);
  
  
  PushButtonStatusWakeup = SET;
  PushButtonStatusData = RESET;
  wakeupCounter = LPM_WAKEUP_TIME; 
  dataSendCounter = DATA_SEND_TIME;
  dataSendCounter++;
#endif

  if(GPIO_Pin==spirit_rx.spirit_spi->RADIO_GPIO_PIN[RADIO_GPIO_3])
  {
    P2PInterruptHandler(&spirit_rx);
  }
  if(GPIO_Pin==spirit_tx.spirit_spi->RADIO_GPIO_PIN[RADIO_GPIO_3])
  {
    P2PInterruptHandler(&spirit_tx);
  }
    
}
/**
* @}
*/
void TimeOutControl(RadioDriver_t *pRadioDriver)
{
  if(pRadioDriver->exitTime)
  {
    /*Decreament the counter to check when 3 seconds has been elapsed*/  
    pRadioDriver->exitCounter--;
    /*3 seconds has been elapsed*/
    if(pRadioDriver->exitCounter <= TIME_UP)
    {
        pRadioDriver->exitTime = RESET;
    }
  }
}
/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
