#include <stdint.h>

#include "tim.h"

volatile uint32_t TIMERS[TIMERS_SIZE];                                          //DEC_TIMERS fonksiyonu 1ms'de bir çağrılarak dizinin elemanlarının değeri 1 azaltılır. 
                                                                                //Gerekli olan TIMER'ları tanımladığımız TIMER dizisi DEC_TIMERS fonksiyonu ile birlikte kullanılır.
//----------------------------------------------------------------------------//
//                        DECREMENT TIMER'S VALUE                             //
//----------------------------------------------------------------------------//
void DEC_TIMERS(void)
{
  int i;
  for(i=0; i<TIMERS_SIZE; i++)
    if(TIMERS[i])
      TIMERS[i]--;
}