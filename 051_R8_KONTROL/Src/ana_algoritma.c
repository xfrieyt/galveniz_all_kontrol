#include "ana_algoritma.h"

#define CIFT_KONTROL_DURUMU 0 // doldur bosalt sirasinda islemi biten tamburu 2.istasyonda beklettir


///////////////////ISTASYON DEGISKENLERI///////////////////////////////////////
uint8_t gidilecek_istasyon_no = 1 ;
uint8_t gidis_yonu = 0 ;
uint8_t istasyon_no = 255;
uint8_t old_istasyon_no = 255 ;
uint8_t gidilecek_istasyon_adedi = 0 ;
uint8_t orta_sensor_kontrol_flag = 0 ;
uint8_t aktif_istasyon_farki = 0; 
extern Vinc_Durumu_t vinc_durumu;
uint8_t orta_sensor_kacirdi = 0; 
//////////////////////////////////////////////////////////////////////////////
extern uint16_t kontrol_timing[6]; 
extern RadioDriver_t *pRadioDriver_rx;

uint8_t t_asagi_inis_tamamlandi = 0; 
uint8_t t_yukari_cikis_tamamlandi = 0 ;

uint8_t asagi_stop_kontrol_flag_u1 = 0 ;
uint8_t yukari_stop_kontrol_flag_u1 = 0 ;

uint8_t acil_stop_bilgisi_gonder = 0 ;

uint8_t hata_kodu_bilgisi_gonder = 0;
uint8_t hata_kodu = 0 ;

uint8_t tambur_tum_deger_gonder = 0;
uint8_t tum_degerleri_gonderilecek_tambur_no = 0 ;

uint8_t tambur_aktif_zaman_degeri_gonder = 0 ;
uint8_t aktif_degeri_gonderilecek_tambur_no = 0 ;

uint8_t oto_asagi_inildi_bilgisi_gonder=0;
uint8_t oto_yukari_cikildi_bilgisi_gonder=0;
uint8_t istasyon_bilgisi_gonder= 0;
uint8_t istasyona_git_basladi_gonder=0;
uint8_t oto_yukari_cik_basladi_gonder=0;
uint8_t oto_asagi_in_basladi_gonder=0;
uint8_t vinc_durumu_gonder = 0;
uint8_t otomatik_cevrim_basladi_gonder = 0 ; 
uint8_t is_bitirme_aktif_gonder = 0 ; 
uint8_t tambur_durumu_gonder = 0 ;
uint8_t durumu_gonderilecek_tambur_no = 0 ;
uint8_t is_bitti_gonder = 0 ; 
uint8_t resetlendi_gonder = 0 ; 
uint8_t ise_baslama_aktif_gonder  = 0 ;
uint8_t tambur_sayaci_basladi_gonder = 0 ; 
uint8_t sayac_bilgisi_gonderilecek_tambur_no = 0;

uint32_t tx_count=0; // rf gelen
uint32_t rx_count=0; // rf giden

uint8_t acil_stop_aktif = 0 ; 

uint8_t tambur_aktiflik_durumu[9] = {0} ;
////////////////OTOMATIK CEVRIM/////////////////////////////////////////////////
Genel_loop t_state = Ilk_Kontrol;


uint8_t tambur_artirma_aktif = false;
uint8_t tambur_kontrol_no = 0;



tambur_time tambur_zamanlama[9] = {0};

typedef enum 
{
  istasyona_git,
  istasyona_gitmeyi_bekle,
  yukari_cik,
  yukari_cikmayi_bekle,
  istasyon_7_ye_git,
  istasyon_7_ye_gitmeyi_bekle,
  _7_istasyon_asagi_in,
  _7_istasyon_asagi_inmeyi_bekle,
  _7_asagida_bekleme_suresi_baslat,
  _7_istasyon_yukari_cikma,
  _7_istasyon_yukari_cikmayi_bekle,
  istasyon_6_ye_git,
  istasyon_6_ye_gitmeyi_bekle,
  _6_istasyon_asagi_in,
  _6_istasyon_asagi_inmeyi_bekle,
  _6_asagida_bekleme_suresi_baslat,
  _6_istasyon_yukari_cikma,
  _6_istasyon_yukari_cikmayi_bekle,
  istasyon_5_ye_git,
  istasyon_5_ye_gitmeyi_bekle,
  _5_istasyon_asagi_in,
  _5_istasyon_asagi_inmeyi_bekle,
  _5_asagida_bekleme_suresi_baslat,
  _5_istasyon_yukari_cikma,
  _5_istasyon_yukari_cikmayi_bekle,
  istasyon_4_ye_git,
  istasyon_4_ye_gitmeyi_bekle,
  _4_istasyon_asagi_in,
  _4_istasyon_asagi_inmeyi_bekle,
  _4_asagida_bekleme_suresi_baslat,
  _4_istasyon_yukari_cikma,
  _4_istasyon_yukari_cikmayi_bekle,
  istasyon_3_ye_git,
  istasyon_3_ye_gitmeyi_bekle,
  _3_istasyon_asagi_in,
  _3_istasyon_asagi_inmeyi_bekle,
  _3_asagida_bekleme_suresi_baslat,
  _3_istasyon_yukari_cikma,
  _3_istasyon_yukari_cikmayi_bekle,
  istasyon_2_ye_git,
  istasyon_2_ye_gitmeyi_bekle,
  _2_istasyon_asagi_in,
  _2_istasyon_asagi_inmeyi_bekle,
  _2_asagida_bekleme_suresi_baslat,
  bitirme_kontrol,
  _2_istasyon_yukari_cikma,
  _2_istasyon_yukari_cikmayi_bekle,
  istasyon_1_ye_git,
  istasyon_1_ye_gitmeyi_bekle,
  _1_istasyon_asagi_in,
  _1_istasyon_asagi_inmeyi_bekle,          
  start_veya_zaman_bekle,
  zaman_bekle,
  ek_kontrol_ayarlamalari, // gereksiz sil
  _1_istasyon_yukari_cik,
  _1_istasyon_yukari_cikmayi_bekle,
  tambur_yerine_git,
  tambur_yerine_gitmeyi_bekle,
  tambur_birak,
  tambur_birakmayi_bekle,
  _bekleyen_tambura_git, // aslinda 1.istasyon
  _bekleyen_tambura_gitmeyi_bekle,
  _bekleyen_tambur_start_bekle,
  _bekleyen_tambur_yukari_kaldir,
  _bekleyen_tambur_yukari_kaldirmayi_bekle,
  _bekleyen_tambur_geri_gotur,
  _bekleyen_tambur_geri_goturmeyi_bekle,
  _bekleyen_tambur_birak,
  _bekleyen_tambur_birakmayi_bekle,
  _2_de_bekleyene_git,
  _2_de_bekleyene_gitmeyi_bekle,
  _2_de_bekleyen_yukari_kaldir,
  _2_de_bekleyen_yukari_kaldirmayi_bekle,
  _2_de_bekleyen_istasyon_1_e_gotur,
  _2_de_bekleyen_istasyon_1_e_goturmeyi_bekle,
  _2_de_bekleyen_istasyon_1_de_asagi_in,
  _2_de_bekleyen_istasyon_1_de_asagi_inmeyi_bekle,
  bitir,
  
}otomatik_cevrim_tambur_kontrol_state;

enum 
{
  i_b_istasyona_git,
  i_b_istasyona_gitmeyi_bekle,
  i_b_yukari_cik,
  i_b_yukari_cikmayi_bekle,
  i_b_istasyon_7_ye_git,
  i_b_istasyon_7_ye_gitmeyi_bekle,
  i_b__7_istasyon_asagi_in,
  i_b__7_istasyon_asagi_inmeyi_bekle,
  i_b__7_asagida_bekleme_suresi_baslat,
  i_b__7_istasyon_yukari_cikma,
  i_b__7_istasyon_yukari_cikmayi_bekle,
  i_b_istasyon_1_e_git,
  i_b_istasyon_1_e_gitmeyi_bekle,
  i_b__1_istasyon_asagi_in,
  i_b__1_istasyon_asagi_inmeyi_bekle,
  i_b_start_bekle,
  i_b__1_istasyon_yukari_cik,
  i_b__1_istasyon_yukari_cikmayi_bekle,
  i_b_istasyon_8_e_git,
  i_b_istasyon_8_e_gitmeyi_bekle,
  i_b__8_istasyon_asagi_in,
  i_b__8_istasyon_asagi_inmeyi_bekle,
  i_b__8_asagida_bekleme_suresi_baslat,
  i_b__8_istasyon_yukari_cikma,
  i_b__8_istasyon_yukari_cikmayi_bekle,
  i_b_istasyona_don,
  i_b_istasyona_donmeyi_bekle,
  i_b_tanbur_birakma,
  i_b_tanbur_birakma_bekle,
  i_b_bitir,
}tambur_i_b_state = i_b_istasyona_git;

struct otomatik_cevrim_degiskenleri
{
            uint8_t islem_basladi;
            uint8_t kontrol_durumu; // ilk otomatik cevrim ise 0, eger start beklerken baska bir tambur aldiysa 1
            uint8_t aktif_tambur_no;
            uint8_t bekleyen_tambur_no;
            otomatik_cevrim_tambur_kontrol_state o_state;
            uint8_t is_bitirme_aktif;
}oto_vars;


uint8_t start_flag = 0 ; 
uint8_t otomatik_cevrim_start = 0 ; 
uint8_t stop_flag = 0 ; // baslayan cevrim i durdurur.

/////////////////OTOMATIK CEVRIM////////////////////////////////////////////////

enum TANBUR_ASAGI_OTOMAT
{
  
 t_asagi_in,
 t_asagi_stop,
 t_en_asagi_in,
 t_asagi_son,

}tanbur_asagi_state = t_asagi_in ;

enum TANBUR_YUKARI_OTOMAT
{   
 t_yukari_cik,
 t_yukari_stop,
 t_tanbur_dondur,
 t_tanbur_durdur,
 t_yukari_cik_bitir,
 t_yukari_son,

}tanbur_yukari_state = t_yukari_cik ;



/*                            */
uint8_t flaggs = 0 ; 
uint8_t ressss= 0 ;
uint8_t flag_timer= 0 ;
//////////////////////////////////
void ana_kontrol()
{

  if(pRadioDriver_rx->RF_Data_Geldi)
  {
    RF_Gelen_Paket_Cozumle(pRadioDriver_rx->RF_usage_buffer);
    pRadioDriver_rx->RF_Data_Geldi = 0 ;
  }
  
  istasyon_no_kontrol();
  sinyal_durumu();
  if(otomatik_cevrim_start)
  {
    if(stop_flag == 0) // stop a basildiysa islem yapmasin
    {
     otomatik_cevrim_fonksiyonu();  
    }   
  }
  
  Kablosuz_Veri_Gonderme_Islemi(); 
}
void sinyal_durumu()
{
  if(orta_sensor_kacirdi == 1)
  {
   Uyari_1_ON;
  }
  else 
  {
   Uyari_1_OFF;
  }
  if(otomatik_cevrim_start == true)
  {
   Uyari_2_ON;
  }
  else
  {
   Uyari_2_OFF;
  }
  
  
}
void _10_Hz_Ayarla()
{
  Surucu_Sinyal_2_ON;
  Surucu_Sinyal_1_OFF;
}

void _45_Hz_Ayarla()
{
  Surucu_Sinyal_2_OFF;
  Surucu_Sinyal_1_OFF; 
}

void _70_Hz_Ayarla()
{
  Surucu_Sinyal_1_ON;
  Surucu_Sinyal_2_OFF;
}

uint8_t sss = 0 ; 
uint8_t ilk_istasyn_ulasildi_kontrol = false;
uint8_t son_istasyn_ulasildi_kontrol = false;

void istasyon_no_kontrol()
{
  if(g_bt_sag_sens.pushFlag && g_bt_orta_sens.pushFlag && ilk_istasyn_ulasildi_kontrol == false )
  {
    istasyon_no = 1 ;
    Yatay_Stop();
    ilk_istasyn_ulasildi_kontrol = true;
    ResetAndStopSens(&Sag_Output);
  }
  if(g_bt_sol_sens.pushFlag && g_bt_orta_sens.pushFlag && son_istasyn_ulasildi_kontrol == false )
  {
    istasyon_no = 17 ;
    Yatay_Stop();
    son_istasyn_ulasildi_kontrol = true;
    ResetAndStopSens(&Sol_Output);
  }
   
  if(g_bt_sol_sens.pullFlag)
  {
    if(istasyon_no == 255) return;
    if(gidis_yonu == SOL_YONE)
    {
      if(istasyon_no < 17)
      istasyon_no++;
      ilk_istasyn_ulasildi_kontrol = false;
    }
    else if(gidis_yonu == SAG_YONE)
    {
      // NOTHING TO DO
    }
    clear_flag(&g_bt_sol_sens);   
  }
  if(g_bt_sag_sens.pullFlag)
  {
    if(istasyon_no == 255) return ;
    if(gidis_yonu == SOL_YONE)
    {
      // NOTHING TO DO
    }
    else if(gidis_yonu == SAG_YONE)
    {
      if(istasyon_no > 1)
      istasyon_no--;
      son_istasyn_ulasildi_kontrol = false;
    }       
    clear_flag(&g_bt_sag_sens);     
  }
  if(old_istasyon_no != istasyon_no)
  {
    old_istasyon_no = istasyon_no;
    istasyon_bilgisi_gonder = 1;
    sss++;
  }
}
uint8_t istasyona_git_kontrolu() // istasyona gitmeyi bekle altinda if icinde kontrol et
{
  uint8_t result = 0 ; 
  if(gidis_yonu == SAG_YONE)
  {
   gidilecek_istasyon_adedi =istasyon_no - gidilecek_istasyon_no ;
  }
  else if(gidis_yonu == SOL_YONE)
  {
   gidilecek_istasyon_adedi = gidilecek_istasyon_no -istasyon_no ;
  }
  if(gidilecek_istasyon_adedi <= 2)
  {
   _45_Hz_Ayarla(); 
  }
  if(gidilecek_istasyon_no == istasyon_no &&  orta_sensor_kontrol_flag == 0)
  {
    orta_sensor_kontrol_flag = 1 ;
    Yatay_Stop();
    TIMERS[orta_sensor_kontrol_gecikmesi] = 1800;
    
  }
  if(orta_sensor_kontrol_flag == 1 && TIMERS[orta_sensor_kontrol_gecikmesi] == 0)
  {
    if(g_bt_orta_sens.pushFlag )
    {
      clear_flag(&g_bt_orta_sens);
      result = 1 ;
      orta_sensor_kontrol_flag = 0 ; 
    }
    else 
    {
     clear_flag(&g_bt_orta_sens);
     orta_sensor_kacirdi = 1 ;
     result = 0; // orta sensor kacirdi
     stop_flag = 1 ;
     hata_kodu_gonder(ORTA_SENSOR_HATASI);  
     orta_sensor_kontrol_flag = 1 ;
    }
    
  }
  
  
  return result;
}
void istasyona_gonder(uint8_t hedef_istasyon)
{
  if(!(hedef_istasyon > 0 && hedef_istasyon < 18)){return ;}
  _70_Hz_Ayarla();
  gidilecek_istasyon_no = hedef_istasyon; 
  if(istasyon_no != hedef_istasyon)
  {
    orta_sensor_kontrol_flag =0;
    gidilecek_istasyon_no = hedef_istasyon;        
    if(istasyon_no < gidilecek_istasyon_no )
    {
      gidis_yonu = SOL_YONE;
      sola_git();           
    }
    else if(istasyon_no > gidilecek_istasyon_no)
    {
      gidis_yonu = SAG_YONE;
      saga_git();  
    }
  }
  else
  {          
    _45_Hz_Ayarla();          
  }
  
}
void oto_yukari_cik_basla() // simdilik fazlalik . rezerve tutuluyor
{
  t_yukari_cikis_tamamlandi = 0 ;                 
  tanbur_yukari_state = t_yukari_cik;
  
}
void oto_asagi_in_basla()  // simdilik fazlalik . rezerve tutuluyor
{
  t_asagi_inis_tamamlandi = 0;
  tanbur_asagi_state = t_asagi_in;
  
}

uint8_t is_count=0;
extern uint32_t counter;
uint32_t send,receive;

void Kablosuz_Veri_Gonderme_Islemi()
{
  if(TIMERS[Kablosuz_Gonderme_Gecikmesi] != 0)
  {
   return;  
  }
  else if(otomatik_cevrim_basladi_gonder > 0)
  {
   Kablosuz_Veri_Gonder(OTOMATIK_CEVRIM_AKTIF,0,0);
   otomatik_cevrim_basladi_gonder--;
  }
  else if(acil_stop_bilgisi_gonder > 0)
  {
    Kablosuz_Veri_Gonder(ACIL_STOP_DURUMU,acil_stop_aktif,0);
    acil_stop_bilgisi_gonder --;
  }
  else if(istasyon_bilgisi_gonder > 0 )
  {
    Kablosuz_Veri_Gonder(ISTASYON_NO_e,istasyon_no,0);
    istasyon_bilgisi_gonder --;
  }
  else if(hata_kodu_bilgisi_gonder > 0)
  {
   Kablosuz_Veri_Gonder(HATA_KODU_e,hata_kodu,0);
   hata_kodu_bilgisi_gonder--;
  }
  else if(tambur_aktif_zaman_degeri_gonder >0)
  {
   uint8_t t_index =  aktif_degeri_gonderilecek_tambur_no - 9 ;
   Kablosuz_Veri_Gonder(TAMBUR_AKTIF_ZAMAN,aktif_degeri_gonderilecek_tambur_no,Convert_Aktif_Zaman_to_Dakika(tambur_zamanlama[t_index].Time));
   tambur_aktif_zaman_degeri_gonder--;

  }
  else if(tambur_tum_deger_gonder > 0)
  {
   uint8_t t_index =  tum_degerleri_gonderilecek_tambur_no - 9 ;  
   Kablosuz_Zaman_Verisi_Aktar(tambur_zamanlama[t_index],tum_degerleri_gonderilecek_tambur_no);
   tambur_tum_deger_gonder--;

  }
  else if(tambur_durumu_gonder > 0)
  {
   Kablosuz_Veri_Gonder(TAMBUR_AKTIFLIK_DURUMU,durumu_gonderilecek_tambur_no,tambur_aktiflik_durumu[durumu_gonderilecek_tambur_no-9]);
   tambur_durumu_gonder--;
  }
  else if(is_bitirme_aktif_gonder > 0)
  {
   Kablosuz_Veri_Gonder(IS_BITIRME_AKTIF,0,0);
   is_bitirme_aktif_gonder--;
  }
  else if(tambur_sayaci_basladi_gonder > 0)
  {
   Kablosuz_Veri_Gonder(TAMBUR_SAYACI_BASLADI,sayac_bilgisi_gonderilecek_tambur_no,tambur_zamanlama[sayac_bilgisi_gonderilecek_tambur_no-9].first_time);
   tambur_sayaci_basladi_gonder--;
  }
  else if(resetlendi_gonder > 0)
  {
   Kablosuz_Veri_Gonder(RESETLENDI,0,0); 
   resetlendi_gonder--;
  }
  else if(is_bitti_gonder >0) 
  {
   Kablosuz_Veri_Gonder(IS_BITTI,0,0); 
   is_bitti_gonder--; 
  }
  else if(ise_baslama_aktif_gonder >0)
  {
   Kablosuz_Veri_Gonder(ISE_BASLAMA_AKTIF,0,0); 
   ise_baslama_aktif_gonder -- ;
  }
}

void sinirAnahtarlariniKontrolEt()
{
  if(g_bt_acil_stop.pushFlag)
  {
   acil_stop_aktif = 1;
   acil_stop_bilgisi_gonder= 1;
   clear_flag(&g_bt_acil_stop);
  }
  else if(g_bt_acil_stop.pullFlag)
  {
   acil_stop_aktif = 0;
   clear_flag(&g_bt_acil_stop);
  }
  if(g_bt_tanbur_asagi.pushFlag && asagi_stop_kontrol_flag_u1 == 0)
  {
    vinc_durumu = Vinc_Asagida;
    Dikey_stop();
    asagi_stop_kontrol_flag_u1 = 1; 
    ResetAndStopSens(&V_Asagi_Output);
  }
  if(g_bt_tanbur_yukari.pushFlag && yukari_stop_kontrol_flag_u1 == 0)
  {
    vinc_durumu = Vinc_Yukarida; 
    Dikey_stop();
    yukari_stop_kontrol_flag_u1 = 1;
    ResetAndStopSens(&V_Yukari_Output);
  }
  if(g_bt_tanbur_asagi.pullFlag)
  {
    asagi_stop_kontrol_flag_u1 = 0 ;
    clear_flag(&g_bt_tanbur_asagi); 
    
  }
  if(g_bt_tanbur_yukari.pullFlag)
  {
    yukari_stop_kontrol_flag_u1 = 0;
    clear_flag(&g_bt_tanbur_yukari);
    
  }
}
void RF_Gelen_Paket_Cozumle(uint8_t* pckt )
{
  if(pckt[SYN1_ADRES] != SYN1 ) return ;
  if(pckt[SYN2_ADRES] != SYN2 ) return ;
  if(pckt[2] == TAMBUR_TUM_DEGERLER_KAYDET)
  {
   if(pckt[z_SYN3_ADRES] != SYN3 ) return ;
  }
  else
  {
   if(pckt[SYN3_ADRES] != SYN3 ) return ;
  }
  
  rx_count++;
  if(acil_stop_aktif )
  {
   acil_stop_bilgisi_gonder= 1;
   return ;
  }
  switch(pckt[FUNCTION_ID_ADRES])
  {
  case OTOMATIK_KONTROL_BASLA:
    otomatik_cevrim_basladi_gonder = 1 ;
    otomatik_cevrim_degiskenleri_sifirla();
    otomatik_cevrim_start = 1 ; 
    break;  
    
  case TAMBUR_ZAMAN_KAYDET:
    if(tambur_zamanlama[pckt[3]-9].IsRunning)
    {
    tambur_zamanlama[pckt[3]-9].Time = Convert_minute_to_millisecond(pckt[4]);
    tambur_aktif_deger_gonder(pckt[3]);
    }
    
    break;
  case TAMBUR_TUM_DEGERLER_KAYDET: 
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].i_7_time    = pckt[z_IST_7_ADRES];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].i_6_time    = pckt[z_IST_6_ADRES];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].i_5_time    = pckt[z_IST_5_ADRES];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].i_4_time    = pckt[z_IST_4_ADRES];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].i_3_time    = pckt[z_IST_3_ADRES];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].i_2_time    = pckt[z_IST_2_ADRES];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].first_time  = pckt[z_IST_OWN];
    tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].t_flag.flag = pckt[z_FLAG_ADRES];
    tambur_aktiflik_durumu[pckt[z_TAMBUR_NO_ADRES]-9] = (uint8_t)tambur_zamanlama[pckt[z_TAMBUR_NO_ADRES]-9].t_flag.bits.aktiflik;
    tambur_tum_degerleri_gonder(pckt[z_TAMBUR_NO_ADRES]);
    break;
  case TAMBUR_TUM_DEGERLER_ISTEK:
    tambur_tum_degerleri_gonder(pckt[3]);
    break;
  case TAMBUR_AKTIF_SAYAC_ISTEK:
    tambur_aktif_deger_gonder(pckt[3]);
    break;
  case ACIL_STOP_SCADA_e:
    Dikey_stop();
    Tambur_Dur();
    Yatay_Stop();
    otomatik_cevrim_degiskenleri_sifirla();
    break;
  case ACIL_STOP_SCADA_IPTAL_e:
    
    break;
  case TAMBUR_AKTIFLIK_DURUMU_KAYDET:
    tambur_aktiflik_durumu[pckt[3] - 9] = pckt[4];
    tambur_aktiflik_durumu_gonder(pckt[3],tambur_aktiflik_durumu[pckt[3] - 9]);
    if(t_state == otomatik_cevrim &&  pckt[4] == 1)  // false durumundan true ya cekerse  // 1 gonderdi zaman basladi 
    {
      if(tambur_zamanlama[pckt[3] - 9].isPaused && tambur_zamanlama[pckt[3] - 9].Time !=0 )
      {
        tambur_zamanlama[pckt[3] - 9].isPaused = false;
        tambur_zamanlama[pckt[3] - 9].IsRunning = true;
      }
      else if(tambur_zamanlama[pckt[3] - 9].isPaused && tambur_zamanlama[pckt[3] - 9].Time ==0 )
      {
        tambur_time_SetIstasyon_time(pckt[3],0);
      }
      else
      {
       tambur_time_SetIstasyon_time(pckt[3],0);
      }           
    }
    else if(t_state == otomatik_cevrim &&  pckt[4] == 0)  // true durumundan false a cekerse // 0 gonderdi zaman durdu 
    {
      tambur_time_pause(pckt[3]);
    }
    break;
  case ISTASYON_KAYDET:
    if(pckt[3] <18 && pckt[3]>0)
    {
     istasyon_no = pckt[3];
    }
    break;
  case START_BUTTON:
    start_flag = 1 ;
    
    if(gidilecek_istasyon_no != istasyon_no && otomatik_cevrim_start == 1 && stop_flag == 1)
    {
      istasyona_gonder(gidilecek_istasyon_no);
      start_flag = 0 ;
      
    }
    else if(otomatik_cevrim_start == 0)
    {
     start_flag = 0 ;
    }
    else if(orta_sensor_kacirdi == 1 )
    {
     orta_sensor_kontrol_flag = 0 ;
    }
    stop_flag = 0;
    break;
  case STOP_BUTTON:
    Dikey_stop();
    Tambur_Dur();
    Yatay_Stop();
    stop_flag = 1 ;
    break;
  case IS_BITIR:
    oto_vars.is_bitirme_aktif = 1 ; 
    is_bitirme_aktif_gonder = 1 ; 
    break;
  case RESET_AT:
    otomatik_cevrim_degiskenleri_sifirla();
    Dikey_stop();
    Tambur_Dur();
    Yatay_Stop();
    resetlendi_gonder = 1 ;
    break;
  case MANUEL_VINC_SOL_e:
    _45_Hz_Ayarla();
    gidis_yonu = SOL_YONE;
    if(g_bt_sol_sens.pushFlag == 1 && g_bt_orta_sens.pushFlag == 1) // sinir kontrol 
    {
     // NULL
     
    }
    else 
    {
     ResetAndStartSens(&Sol_Output);
    }
    break;
  case MANUEL_VINC_SAG_e:
    _45_Hz_Ayarla();
    gidis_yonu = SAG_YONE;
    if(g_bt_sag_sens.pushFlag == 1 && g_bt_orta_sens.pushFlag == 1) // sinir kontrol 
    {
     
    }
    else 
    {
     ResetAndStartSens(&Sag_Output);
    }
    break;
  case MANUEL_VINC_YUKARI_e:
    if(g_bt_tanbur_yukari.pushFlag != 1 )
    {
      ResetAndStartSens(&V_Yukari_Output);
    }
   
    break;
  case MANUEL_VINC_ASAGI_e:
    if(g_bt_tanbur_asagi.pushFlag != 1)
    {
     ResetAndStartSens(&V_Asagi_Output);
    }   
    break;
  case MANUEL_VINC_YATAY_STOP_e:
    Yatay_Stop();
    ResetAndStopSens(&Sol_Output);
    ResetAndStopSens(&Sag_Output);
    break;
  case MANUEL_VINC_DIKEY_STOP_e:
    Dikey_stop();
    ResetAndStopSens(&V_Yukari_Output);
    ResetAndStopSens(&V_Asagi_Output);
    break;
     
  }
  
}


uint8_t Oto_Asagi_in_kontrolu() //if icinde kontrol et
{
  uint8_t result=0;
  if(g_bt_tanbur_asagi.pushFlag == 1 && tanbur_asagi_state != t_asagi_son )
  {
    t_asagi_inis_tamamlandi = 0;
    tanbur_asagi_state = t_asagi_in;
    oto_asagi_inildi_bilgisi_gonder = 1 ;
    Dikey_stop();
    result = 1;
  }
  switch(tanbur_asagi_state)
  {       
  case  t_asagi_in:
    if(TIMERS[tanbur_asagi_kontrol] == 0) 
    {
      Asagi_In();
      if(istasyon_no == 1 )
      {
        tanbur_asagi_state = t_asagi_son;
      }
      else
      {
        TIMERS[tanbur_asagi_kontrol] = asagi_inis_suresi;  
        tanbur_asagi_state = t_asagi_stop;
      }
    }
    break;
  case  t_asagi_stop:
    if(TIMERS[tanbur_asagi_kontrol] == 0)
    {
      Dikey_stop();
      TIMERS[tanbur_asagi_kontrol] = asagida_ilk_kalma_suresi;
      tanbur_asagi_state = t_en_asagi_in;
    }
    break;
  case t_en_asagi_in:
    if(TIMERS[tanbur_asagi_kontrol] == 0)
    {
      Asagi_In();
      tanbur_asagi_state = t_asagi_son;
    }
    break;
  case t_asagi_son:
    if(g_bt_tanbur_asagi.pushFlag == 1 && t_asagi_inis_tamamlandi == 0 )
    {
      TIMERS[tanbur_asagi_kontrol] = asagida_son_kalma_suresi;        
      t_asagi_inis_tamamlandi = 1;
      Dikey_stop();      
    }
    else if(g_bt_tanbur_asagi.pushFlag == 0 && t_asagi_inis_tamamlandi == 0)
    {
      Asagi_In();
    }
    else if(t_asagi_inis_tamamlandi == 1 )
    {
      if(TIMERS[tanbur_asagi_kontrol] == 0)
      {
        t_asagi_inis_tamamlandi = 0;
        tanbur_asagi_state = t_asagi_in;
        oto_asagi_inildi_bilgisi_gonder = 1 ;  // KONTROL ET*****
        result = 1;
      }
    }
    
    break;      
  }
  
  
  
  return result;
}

uint8_t Oto_Yukari_Cik_kontrol() // if icinde kontrol et
{
  uint8_t result = 0;
  if(g_bt_tanbur_yukari.pushFlag == 1 && tanbur_yukari_state != t_yukari_son)
  {
    t_yukari_cikis_tamamlandi = 0 ;                 
    oto_yukari_cikildi_bilgisi_gonder = RETRY_SEND ; // KONTROL ET*****     
    //state = STAND_BY ;
    tanbur_yukari_state = t_yukari_cik; 
    Dikey_stop();
    result =1 ;
    return result;
  }
  switch(tanbur_yukari_state)
  {
  case  t_yukari_cik:
    if(TIMERS[tanbur_yukari_kontrol] == 0)
    {
      Yukari_Cik();
      if(istasyon_no == 1 )
      {
        tanbur_yukari_state = t_yukari_son;           
      }
      else
      {
        TIMERS[tanbur_yukari_kontrol] = yukari_cikma_suresi;
        tanbur_yukari_state = t_yukari_stop;
      }       
    }
    break;
  case  t_yukari_stop:
    if(TIMERS[tanbur_yukari_kontrol] == 0)
    {
      Dikey_stop();
      TIMERS[tanbur_yukari_kontrol] = 100;
      tanbur_yukari_state = t_tanbur_dondur;
    }
    break;
  case  t_tanbur_dondur:
    if(TIMERS[tanbur_yukari_kontrol] == 0)
    {
      Tambur_Dondur();    
      TIMERS[tanbur_yukari_kontrol] = 2500;
      tanbur_yukari_state = t_tanbur_durdur;
    }
    
    break;
  case  t_tanbur_durdur:       
    if(TIMERS[tanbur_yukari_kontrol] == 0)
    {
      Tambur_Dur();
      TIMERS[tanbur_yukari_kontrol] = 100;
      tanbur_yukari_state = t_yukari_cik_bitir;
    }
    break;
    
  case  t_yukari_cik_bitir:
    if(TIMERS[tanbur_yukari_kontrol] == 0)
    {
      Yukari_Cik();
      tanbur_yukari_state = t_yukari_son ;
      
    }
    break;
  case t_yukari_son:
    if(g_bt_tanbur_yukari.pushFlag == 1 &&  t_yukari_cikis_tamamlandi == 0 )
    {
      t_yukari_cikis_tamamlandi = 1; 
      if(istasyon_no != 1)
      {
       TIMERS[tanbur_yukari_kontrol] = 6500 ;
      }
      else
      {
       TIMERS[tanbur_yukari_kontrol] = 100 ;
      }
      
      Dikey_stop();
    }
    if(g_bt_tanbur_yukari.pushFlag == 1 &&  t_yukari_cikis_tamamlandi == 0 )
    {
      Yukari_Cik();
    }
    else if(t_yukari_cikis_tamamlandi == 1)
    {
      if(TIMERS[tanbur_yukari_kontrol] == 0)
      {
        t_yukari_cikis_tamamlandi = 0 ;                 
        oto_yukari_cikildi_bilgisi_gonder = RETRY_SEND ;
        tanbur_yukari_state = t_yukari_cik;        
        result =1 ;
       
      }
    }
    break;
  }
  return result;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint16_t Convert_Aktif_Zaman_to_Dakika(uint32_t millisecond)// ozel islem var  rastgele kullanma
{
 uint8_t dakika =  (millisecond / 60000);
 if(dakika != 0)
 {
   return dakika ;

 }
 else 
 {
  uint8_t temp_saniye =  millisecond / 1000 ; 
   
  if(temp_saniye != 0 )
  {
    return (255-temp_saniye);

  }
  else 
  {
    return 0;
  }
 }
 
}
uint16_t Convert_millisecond_to_minute(uint32_t millisecond) // milisaniyeden dakikaya
{
 uint16_t temp = (millisecond / 60000);
 return temp;
}
uint32_t Convert_minute_to_millisecond(uint16_t minute)
{
uint32_t temp = minute * 60000 ;
return temp;
} 
uint32_t Convert_Second_to_Millisecond(uint16_t second )
{
 uint32_t temp = second * 1000 ;
return temp;

}
void tambur_time_SetRaw_time(uint8_t tambur_no,uint32_t time_val) // dogrudan aktif olan time degerini gunceller
{
 uint8_t tambur_no_index = tambur_no - 9 ;
 tambur_zamanlama[tambur_no_index].Time = time_val;
}
void tambur_time_SetIstasyon_time(uint8_t tambur_no,uint8_t istasyon_no ) // ornek olarak 9.tamburun 5.istasyondaki bekleme suresi 
{
 uint8_t tambur_no_index = tambur_no - 9 ;
 switch(istasyon_no)
 {
 case 7:
   tambur_zamanlama[tambur_no_index].Time = Convert_Second_to_Millisecond(tambur_zamanlama[tambur_no_index].i_7_time);
   break;
 case 6:
   tambur_zamanlama[tambur_no_index].Time = Convert_Second_to_Millisecond(tambur_zamanlama[tambur_no_index].i_6_time);
   break;
 case 5:
   tambur_zamanlama[tambur_no_index].Time = Convert_Second_to_Millisecond(tambur_zamanlama[tambur_no_index].i_5_time);
   break;
 case 4:
   tambur_zamanlama[tambur_no_index].Time = Convert_Second_to_Millisecond(tambur_zamanlama[tambur_no_index].i_4_time);
   break;
 case 3 :
   tambur_zamanlama[tambur_no_index].Time = Convert_Second_to_Millisecond(tambur_zamanlama[tambur_no_index].i_3_time);
   break;
 case 2:
   tambur_zamanlama[tambur_no_index].Time = Convert_Second_to_Millisecond(tambur_zamanlama[tambur_no_index].i_2_time);
   break;
 case 0:
   tambur_zamanlama[tambur_no_index].Time = Convert_minute_to_millisecond(tambur_zamanlama[tambur_no_index].first_time);
   break;
 }
 tambur_zamanlama[tambur_no_index].IsRunning = true;
}

uint8_t tambur_time_GetTimerStatus(uint8_t tambur_no)
{
  uint8_t tambur_no_index = tambur_no - 9 ;
  return(tambur_zamanlama[tambur_no_index].Timeout);
}

void tambur_time_ResetTimer(uint8_t tambur_no)
{			
  uint8_t tambur_no_index = tambur_no - 9 ;
  tambur_zamanlama[tambur_no_index].IsRunning = false;
  tambur_zamanlama[tambur_no_index].Time = 0;
  tambur_zamanlama[tambur_no_index].Timeout = false;
  tambur_zamanlama[tambur_no_index].isPaused = false;
}
void tambur_time_pause(uint8_t tambur_no)
{
   uint8_t tambur_no_index = tambur_no - 9 ;
   tambur_zamanlama[tambur_no_index].isPaused = true;
}
uint8_t tambur_time_IsTimeUp(uint8_t tambur_no) // timeup ise sifirlamayi unutma
{
  uint8_t tambur_no_index = tambur_no - 9 ;
  return tambur_zamanlama[tambur_no_index].Timeout ;

}
void tambur_time_ISR(void)
{
  for(int i=0; i < 9 ; i++)
  {
    if(tambur_zamanlama[i].IsRunning)
    {				
      if(tambur_zamanlama[i].Time <= 0) // timer kurulmus ve sifirlanmis ise 
      {
        tambur_zamanlama[i].Timeout = true;	
      }
      else
      {
        if(tambur_zamanlama[i].isPaused == false)
        {
         tambur_zamanlama[i].Time--;
        }
      }		 
    } 
    else 
    {
      tambur_zamanlama[i].Timeout = false;
      tambur_zamanlama[i].Time = 0;
    }
  }
}
uint8_t tambur_istasyon_flag_al(uint8_t tambur_no,uint8_t istasyon_no) // ornek 9.tambur 5.istasyona ugrayacak mi flag ini dondurur.
{
  uint8_t tambur_no_index = tambur_no - 9 ;
  uint8_t result = 0 ; 
  switch(istasyon_no)
  {
  case 7:
    result = tambur_zamanlama[tambur_no_index].t_flag.bits.i_7_f ;
    break;
  case 6:
    result = tambur_zamanlama[tambur_no_index].t_flag.bits.i_6_f ;
    break;
  case 5:
    result =tambur_zamanlama[tambur_no_index].t_flag.bits.i_5_f ;
    break;
  case 4:
    result = tambur_zamanlama[tambur_no_index].t_flag.bits.i_4_f ;
    break;
  case 3 :
    result= tambur_zamanlama[tambur_no_index].t_flag.bits.i_3_f ;
    break;
  case 2:
    result =tambur_zamanlama[tambur_no_index].t_flag.bits.i_2_f ;
    break;
  }
  return result ;
  
}
void otomatik_cevrim_fonksiyonu()
{
  switch (t_state)
  {
  case Ilk_Kontrol:
    IseBaslamaIlkkontrol();
    break;
  case ise_baslama:
    iseBaslamaFonksiyonu();
    break;
  case RF_Sekansi:
    if(TIMERS[otomatik_cevrim_basladi_gecikmesi] == 0)
    {
     t_state = otomatik_cevrim;
     //otomatik_cevrim_basladi_gonder = 1 ;
    }
    break;
  case otomatik_cevrim:
    otomatikCevrimFonksiyonu();
    break;
  default:
    break;
  } 
}

void IseBaslamaIlkkontrol()
{
  if (istasyon_no != 17 && istasyon_no != 1) 
  {
     hata_kodu_gonder(ISTASYON_DURUMU_HATASI);   
     otomatik_cevrim_degiskenleri_sifirla();
     otomatik_cevrim_start = false;
  }
  if (vinc_durumu != Vinc_Asagida) 
  {
      hata_kodu_gonder(VINC_KONUM_HATASI);
      otomatik_cevrim_degiskenleri_sifirla();
      otomatik_cevrim_start = false;
  }
  if (!Tambur_Kontrol()) 
  {
    hata_kodu_gonder(AKTIF_TAMBUR_YOK);
    otomatik_cevrim_degiskenleri_sifirla();
    otomatik_cevrim_start = false;
    
  } 
  if (istasyon_no == 1 && vinc_durumu == Vinc_Asagida)
  {
    t_state = ise_baslama;
    
    tambur_kontrol_no = 9;
    tambur_artirma_aktif = false;
    ise_baslama_aktif_gonder = 1 ;
  }
  
}
uint8_t Tambur_Kontrol() 
{
  uint8_t result = false;
  for(int i =0 ; i< 9; i++ )
  {
    if(tambur_aktiflik_durumu[i] == true)
    {
     result = true;
     break;
    }
  }
  return result;
}
void iseBaslamaFonksiyonu()
{

  switch (tambur_kontrol_no)
  {
  case 9:
    if (tambur_aktiflik_durumu[tambur9devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);                        
    }
    if (tambur_aktiflik_durumu[tambur9devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 10;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git;                      
    }
    break;
  case 10:
    if (tambur_aktiflik_durumu[tambur10devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur10devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 11;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    
    break;
  case 11:
    if (tambur_aktiflik_durumu[tambur11devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur11devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 12;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    
    break;
  case 12:
    if (tambur_aktiflik_durumu[tambur12devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur12devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 13;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    
    break;
  case 13:
    if (tambur_aktiflik_durumu[tambur13devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur13devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 14;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    break;
  case 14:
    if (tambur_aktiflik_durumu[tambur14devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur14devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 15;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    
    break;
  case 15:
    if (tambur_aktiflik_durumu[tambur15devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur15devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 16;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    
    break;
  case 16:
    if (tambur_aktiflik_durumu[tambur16devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur16devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 17;
      tambur_artirma_aktif = false;
      tambur_i_b_state = i_b_istasyona_git; 
    }
    
    break;
  case 17:
    if (tambur_aktiflik_durumu[tambur17devrede] == true)
    {
      ise_baslama_tambur_kontrol_dongusu(tambur_kontrol_no);
    }
    if (tambur_aktiflik_durumu[tambur17devrede] == false || tambur_artirma_aktif == true) 
    {
      tambur_kontrol_no = 9; // resetlendi
      tambur_artirma_aktif = false;
      t_state = RF_Sekansi;
      TIMERS[otomatik_cevrim_basladi_gecikmesi] = 100 ;
      
    }
    
    break;
  default:
    break;
  }
}
void ise_baslama_tambur_kontrol_dongusu(uint8_t tambur_no) 
{
  switch (tambur_i_b_state)
  {
  case i_b_istasyona_git:
    istasyona_gonder(tambur_no);
    tambur_i_b_state = i_b_istasyona_gitmeyi_bekle;
    break;
  case i_b_istasyona_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      tambur_i_b_state = i_b_yukari_cik;
    }
    break;
  case i_b_yukari_cik:
    oto_yukari_cik_basla();
    tambur_i_b_state = i_b_yukari_cikmayi_bekle;
    break;
  case i_b_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      tambur_i_b_state = i_b_istasyon_7_ye_git;
    }    
    break;
  case i_b_istasyon_7_ye_git:
    istasyona_gonder(7);
    tambur_i_b_state = i_b_istasyon_7_ye_gitmeyi_bekle;
    break;
  case i_b_istasyon_7_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      tambur_i_b_state = i_b__7_istasyon_asagi_in;
    }
    break;
  case i_b__7_istasyon_asagi_in:
    oto_asagi_in_basla();
    tambur_i_b_state = i_b__7_istasyon_asagi_inmeyi_bekle;
    break;
  case i_b__7_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      tambur_i_b_state = i_b__7_asagida_bekleme_suresi_baslat;
      TIMERS[ise_baslama_tambur_gecikmesi] = 1000;////////////////////          TIME KONTROL
    }
    break;
  case i_b__7_asagida_bekleme_suresi_baslat: 
    if(TIMERS[ise_baslama_tambur_gecikmesi] ==0)
    {
      tambur_i_b_state = i_b__7_istasyon_yukari_cikma;
    }
    
    break;
  case i_b__7_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    tambur_i_b_state = i_b__7_istasyon_yukari_cikmayi_bekle;
    break;
  case i_b__7_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      tambur_i_b_state = i_b_istasyon_1_e_git;
    }
    break;
  case i_b_istasyon_1_e_git:
    istasyona_gonder(1);
    tambur_i_b_state = i_b_istasyon_1_e_gitmeyi_bekle;
    break;
  case i_b_istasyon_1_e_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      tambur_i_b_state = i_b__1_istasyon_asagi_in;
    }
    break;
  case i_b__1_istasyon_asagi_in:
    oto_asagi_in_basla();
    tambur_i_b_state = i_b__1_istasyon_asagi_inmeyi_bekle;
    break;
  case i_b__1_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      tambur_i_b_state = i_b_start_bekle;
    }
    break;
  case i_b_start_bekle:
    if(start_flag == true)
    {
     tambur_i_b_state = i_b__1_istasyon_yukari_cik;
     start_flag = 0 ;
    }
    break;
  case i_b__1_istasyon_yukari_cik:
    oto_yukari_cik_basla();
    tambur_i_b_state = i_b__1_istasyon_yukari_cikmayi_bekle;
    break;
  case i_b__1_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      tambur_i_b_state = i_b_istasyon_8_e_git;
    }
    break;
  case i_b_istasyon_8_e_git:
    istasyona_gonder(8);
    tambur_i_b_state = i_b_istasyon_8_e_gitmeyi_bekle;
    break;
  case i_b_istasyon_8_e_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      tambur_i_b_state = i_b__8_istasyon_asagi_in;
    }
    break;
  case i_b__8_istasyon_asagi_in:
    oto_asagi_in_basla();
    tambur_i_b_state = i_b__8_istasyon_asagi_inmeyi_bekle;
    break;
  case i_b__8_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      tambur_i_b_state = i_b__8_asagida_bekleme_suresi_baslat;
      TIMERS[ise_baslama_tambur_gecikmesi] = 1000 ;  /////////////////////////// TIME KONTROL
    }
    break;
  case i_b__8_asagida_bekleme_suresi_baslat:
    if(TIMERS[ise_baslama_tambur_gecikmesi] == 0)
    {
      tambur_i_b_state = i_b__8_istasyon_yukari_cikma; 
    }
    break;
  case i_b__8_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    tambur_i_b_state = i_b__8_istasyon_yukari_cikmayi_bekle;
    break;
  case i_b__8_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      tambur_i_b_state = i_b_istasyona_don;
    }
    break;
  case i_b_istasyona_don:
    istasyona_gonder(tambur_no);
    tambur_i_b_state = i_b_istasyona_donmeyi_bekle;
    break;
  case i_b_istasyona_donmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      tambur_i_b_state = i_b_tanbur_birakma;
    }
    break;
  case i_b_tanbur_birakma:
    oto_asagi_in_basla();
    tambur_i_b_state = i_b_tanbur_birakma_bekle;
    break;
  case i_b_tanbur_birakma_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      tambur_i_b_state = i_b_bitir;
    }
    break;
  case i_b_bitir:
    tambur_artirma_aktif = true;
    tambur_time_SetIstasyon_time(tambur_no,0);
    tambur_sayaci_basladi(tambur_no);    
    break;    
  default:
    break;
  }
}
void otomatikCevrimFonksiyonu()
{
  if (oto_vars.islem_basladi == false) 
  {
    otomatikCevrimZamankontrol(); 
  }
  else if (oto_vars.islem_basladi == true) 
  {
    otomatikCevrimKontrolFonksiyonu();  
  }
}
void otomatikCevrimZamankontrol()
{
  for (int i = 0; i < 9; i++) // tambur_sayisi
  {
    if (tambur_time_IsTimeUp(i+9))
    {
      if (oto_vars.islem_basladi == false && oto_vars.kontrol_durumu == 0 )
      {
        oto_vars.islem_basladi = true;
        oto_vars.aktif_tambur_no = (uint8_t)(i + 9);  // 9 dan itibaren diye
        oto_vars.bekleyen_tambur_no = 0;
        oto_vars.o_state = istasyona_git;
        break;
      }
      else if (oto_vars.islem_basladi == false && oto_vars.kontrol_durumu == 1) 
      {
        //CHECK
        break;
      }
      else if (oto_vars.islem_basladi == true ) // ilk durumda kontrol 1 e cekilirken ki durum
      {
        oto_vars.bekleyen_tambur_no = oto_vars.aktif_tambur_no;
        oto_vars.aktif_tambur_no = (uint8_t)(i + 9);
        oto_vars.o_state = istasyona_git;
        if(istasyon_no != 1)
        {
         oto_vars.kontrol_durumu = 0;
        }
        else
        {
         oto_vars.kontrol_durumu = 1;
        }
        
        break;
      }
      //else if (oto_vars.islem_basladi == true && oto_vars.kontrol_durumu == 1) 
      //{
      //  oto_vars.o_state = istasyona_git;
      //  oto_vars.bekleyen_tambur_no = oto_vars.aktif_tambur_no;
      //  oto_vars.aktif_tambur_no = (uint8_t)(i + 9);
      //  oto_vars.kontrol_durumu = 1;
      //  break;
      //}
      tambur_time_ResetTimer(i+9);  
    }
  }
}
void otomatikCevrimKontrolFonksiyonu() 
{
  if (oto_vars.aktif_tambur_no == 0) 
  {
    //MessageBox.Show("HATA");
  }
  if(is_bitirme_kontrolu())
  {
    is_bitti_gonder = 1 ;
    otomatik_cevrim_degiskenleri_sifirla();
  }
  switch (oto_vars.o_state)
  {
  case istasyona_git:
    istasyona_gonder(oto_vars.aktif_tambur_no);
    oto_vars.o_state = istasyona_gitmeyi_bekle;
    break;
  case istasyona_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state = yukari_cik;
    }
    break;
  case  yukari_cik:
    oto_yukari_cik_basla();
    oto_vars.o_state =  yukari_cikmayi_bekle;
    break;
  case  yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_7_ye_git;
    }
    break;
  case  istasyon_7_ye_git:
    if (tambur_istasyon_flag_al(oto_vars.aktif_tambur_no, 7) == 1)
    {
      istasyona_gonder(7);
      oto_vars.o_state =  istasyon_7_ye_gitmeyi_bekle;
    }
    else 
    {
      oto_vars.o_state =  istasyon_6_ye_git;
      
    }
    break;
  case  istasyon_7_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _7_istasyon_asagi_in;
    }
    break;
  case  _7_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _7_istasyon_asagi_inmeyi_bekle;
    break;
  case  _7_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  _7_asagida_bekleme_suresi_baslat;
      tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,7);
    }
    break;
  case  _7_asagida_bekleme_suresi_baslat:
    if (tambur_time_IsTimeUp(oto_vars.aktif_tambur_no))
    {
      oto_vars.o_state =  _7_istasyon_yukari_cikma;
      tambur_time_ResetTimer(oto_vars.aktif_tambur_no);
    }      
    break;
  case  _7_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _7_istasyon_yukari_cikmayi_bekle;
    break;
  case  _7_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_6_ye_git;
    }

    break;
  case  istasyon_6_ye_git:
    if (tambur_istasyon_flag_al(oto_vars.aktif_tambur_no, 6) == 1)
    {
      istasyona_gonder(6);
      oto_vars.o_state =  istasyon_6_ye_gitmeyi_bekle;
    }
    else
    {
      oto_vars.o_state =  istasyon_5_ye_git;
      
    }
    break;
  case  istasyon_6_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _6_istasyon_asagi_in;
    }

    break;
  case  _6_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _6_istasyon_asagi_inmeyi_bekle;
    break;
  case  _6_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  _6_asagida_bekleme_suresi_baslat;
      tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,6);
    }
    break;
  case  _6_asagida_bekleme_suresi_baslat:
    if (tambur_time_IsTimeUp(oto_vars.aktif_tambur_no))   
    {
      oto_vars.o_state =  _6_istasyon_yukari_cikma;
      tambur_time_ResetTimer(oto_vars.aktif_tambur_no);
    } 
    break;
  case  _6_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _6_istasyon_yukari_cikmayi_bekle;
    break;
  case  _6_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_5_ye_git;
    }
    break;
  case  istasyon_5_ye_git:
    if (tambur_istasyon_flag_al(oto_vars.aktif_tambur_no, 5) == 1)
    {
      istasyona_gonder(5);
      oto_vars.o_state =  istasyon_5_ye_gitmeyi_bekle;
    }
    else
    {
      oto_vars.o_state =  istasyon_4_ye_git;
      
    }
    break;
  case  istasyon_5_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _5_istasyon_asagi_in;
    }

    break;
  case  _5_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _5_istasyon_asagi_inmeyi_bekle;
    break;
  case  _5_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  _5_asagida_bekleme_suresi_baslat;
      tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,5);;
    }

    break;
  case  _5_asagida_bekleme_suresi_baslat:
    if (tambur_time_IsTimeUp(oto_vars.aktif_tambur_no))
    {
      oto_vars.o_state =  _5_istasyon_yukari_cikma;
      tambur_time_ResetTimer(oto_vars.aktif_tambur_no);
    } 
    break;
  case  _5_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _5_istasyon_yukari_cikmayi_bekle;
    break;
  case  _5_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_4_ye_git;
    }

    break;
  case  istasyon_4_ye_git:
    if (tambur_istasyon_flag_al(oto_vars.aktif_tambur_no, 4) == 1)
    {
      istasyona_gonder(4);
      oto_vars.o_state =  istasyon_4_ye_gitmeyi_bekle;
    }
    else
    {
      oto_vars.o_state =  istasyon_3_ye_git;
      
    }
    break;
  case  istasyon_4_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _4_istasyon_asagi_in;
    }

    break;
  case  _4_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _4_istasyon_asagi_inmeyi_bekle;
    break;
  case  _4_istasyon_asagi_inmeyi_bekle:  //
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  _4_asagida_bekleme_suresi_baslat;
      tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,4);
    }

    break;
  case  _4_asagida_bekleme_suresi_baslat:
    if (tambur_time_IsTimeUp(oto_vars.aktif_tambur_no))
    {
      oto_vars.o_state =  _4_istasyon_yukari_cikma;
      tambur_time_ResetTimer(oto_vars.aktif_tambur_no);
    } 
    break;
  case  _4_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _4_istasyon_yukari_cikmayi_bekle;
    break;
  case  _4_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_3_ye_git;
    }

    break;
  case  istasyon_3_ye_git:
    if (tambur_istasyon_flag_al(oto_vars.aktif_tambur_no, 3) == 1)
    {
      istasyona_gonder(3);
      oto_vars.o_state =  istasyon_3_ye_gitmeyi_bekle;
    }
    else
    {
      oto_vars.o_state =  istasyon_2_ye_git;
      
    }
    break;
  case  istasyon_3_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _3_istasyon_asagi_in;
    }

    break;
  case  _3_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _3_istasyon_asagi_inmeyi_bekle;
    break;
  case  _3_istasyon_asagi_inmeyi_bekle: //
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  _3_asagida_bekleme_suresi_baslat;
      tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,3);
    }

    break;
  case  _3_asagida_bekleme_suresi_baslat:
    if (tambur_time_IsTimeUp(oto_vars.aktif_tambur_no))
    {
      oto_vars.o_state =  _3_istasyon_yukari_cikma;
      tambur_time_ResetTimer(oto_vars.aktif_tambur_no);
    } 
    break;
  case  _3_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _3_istasyon_yukari_cikmayi_bekle;
    break;
  case  _3_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_2_ye_git;
    }

    break;
  case  istasyon_2_ye_git:
#if CIFT_KONTROL_DURUMU
    istasyona_gonder(2);
    oto_vars.o_state =  istasyon_2_ye_gitmeyi_bekle;
#else 
    if (tambur_istasyon_flag_al(oto_vars.aktif_tambur_no, 2) == 1)
    {
      istasyona_gonder(2);
      oto_vars.o_state =  istasyon_2_ye_gitmeyi_bekle;
    }
    else
    {
     oto_vars.o_state =  istasyon_1_ye_git;
    }
#endif
    break;
  case  istasyon_2_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _2_istasyon_asagi_in;
      
    }

    break;
  case  _2_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _2_istasyon_asagi_inmeyi_bekle;
    break;
  case  _2_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  _2_asagida_bekleme_suresi_baslat;
      tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,2);
    }

    break;
  case  _2_asagida_bekleme_suresi_baslat:
    if (tambur_time_IsTimeUp(oto_vars.aktif_tambur_no))
    {
#if CIFT_KONTROL_DURUMU
      oto_vars.o_state =  bitirme_kontrol;
#else
      oto_vars.o_state =  _2_istasyon_yukari_cikma;   
#endif
      tambur_time_ResetTimer(oto_vars.aktif_tambur_no);
    }   
    break;
  case  bitirme_kontrol:
    if (oto_vars.kontrol_durumu == 0) 
    {
      oto_vars.o_state =  _2_istasyon_yukari_cikma;   
    }
    else if (oto_vars.kontrol_durumu == 1)
    {
      oto_vars.o_state =  _bekleyen_tambura_git;
      
    }
    break;
  case  _2_istasyon_yukari_cikma:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _2_istasyon_yukari_cikmayi_bekle;
    break;
  case  _2_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  istasyon_1_ye_git;
    }

    break;
  case  istasyon_1_ye_git:
    istasyona_gonder(1);
    oto_vars.o_state =  istasyon_1_ye_gitmeyi_bekle;
    break;
  case  istasyon_1_ye_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _1_istasyon_asagi_in;
    }

    break;
  case  _1_istasyon_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _1_istasyon_asagi_inmeyi_bekle;
    break;
  case  _1_istasyon_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      
      oto_vars.o_state =  start_veya_zaman_bekle;  //////// KONTROL
      start_flag = 0;
      
    }

    break;
  case  start_veya_zaman_bekle:
    if (start_flag == true) 
    {
      oto_vars.o_state =  _1_istasyon_yukari_cik; // burdan sonra yerine gider
      start_flag = false;
    }
#if CIFT_KONTROL_DURUMU
    otomatikCevrimZamankontrol();
#endif
    break;
  case  ek_kontrol_ayarlamalari:
    
    break;
  case  _1_istasyon_yukari_cik:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _1_istasyon_yukari_cikmayi_bekle;
    break;
  case  _1_istasyon_yukari_cikmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  tambur_yerine_git;
    }

    break;
  case  tambur_yerine_git:
    istasyona_gonder(oto_vars.aktif_tambur_no); // KONTROL ET
    oto_vars.o_state =  tambur_yerine_gitmeyi_bekle;
    break;
  case  tambur_yerine_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  tambur_birak;
    }

    break;
  case  tambur_birak:
    oto_asagi_in_basla();
    oto_vars.o_state =  tambur_birakmayi_bekle;
    break;
  case  tambur_birakmayi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      if (oto_vars.is_bitirme_aktif == false)
      {        
        tambur_time_SetIstasyon_time(oto_vars.aktif_tambur_no,0); // kendi istasyonundaki bekleme suresi
        tambur_sayaci_basladi(oto_vars.aktif_tambur_no);
      }
      else
      {
        // IS BITIRME AKTIF
      }
      oto_vars.o_state =  zaman_bekle;   
    }

    break;
  case  _bekleyen_tambura_git:
    istasyona_gonder(1); // KONTROL ET
    oto_vars.o_state =  _bekleyen_tambura_gitmeyi_bekle;
    break;
  case  _bekleyen_tambura_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _bekleyen_tambur_start_bekle;
    }

    break;
  case  _bekleyen_tambur_start_bekle:
    if(start_flag)
    {
     oto_vars.o_state =  _bekleyen_tambur_yukari_kaldir;
     start_flag = false;
    }
    break;
  case  _bekleyen_tambur_yukari_kaldir:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _bekleyen_tambur_yukari_kaldirmayi_bekle;
    break;
  case  _bekleyen_tambur_yukari_kaldirmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  _bekleyen_tambur_geri_gotur;
    }

    break;
  case  _bekleyen_tambur_geri_gotur:
    istasyona_gonder(oto_vars.bekleyen_tambur_no); // KONTROL ET
    oto_vars.o_state =  _bekleyen_tambur_geri_goturmeyi_bekle;
    break;
  case  _bekleyen_tambur_geri_goturmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _bekleyen_tambur_birak;
    }

    break;
  case  _bekleyen_tambur_birak:
    oto_asagi_in_basla();
    oto_vars.o_state =  _bekleyen_tambur_birakmayi_bekle;
    break;
  case  _bekleyen_tambur_birakmayi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      if (oto_vars.is_bitirme_aktif == false)
      {       
        tambur_time_SetIstasyon_time(oto_vars.bekleyen_tambur_no,0); // kendi istasyonundaki bekleme suresi
        tambur_sayaci_basladi(oto_vars.bekleyen_tambur_no);
      }
      else
      {
        // IS BITIRME AKTIF
      }
      oto_vars.o_state =  _2_de_bekleyene_git;
    }

    break;
  case  _2_de_bekleyene_git:
    istasyona_gonder(2); // KONTROL ET
    oto_vars.o_state =  _2_de_bekleyene_gitmeyi_bekle;
    break;
  case  _2_de_bekleyene_gitmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _2_de_bekleyen_yukari_kaldir;
    }
    break;
  case  _2_de_bekleyen_yukari_kaldir:
    oto_yukari_cik_basla();
    oto_vars.o_state =  _2_de_bekleyen_yukari_kaldirmayi_bekle;
    break;
  case  _2_de_bekleyen_yukari_kaldirmayi_bekle:
    if (Oto_Yukari_Cik_kontrol()) 
    {
      oto_vars.o_state =  _2_de_bekleyen_istasyon_1_e_gotur;
    }

    break;
  case  _2_de_bekleyen_istasyon_1_e_gotur:
    istasyona_gonder(1); // KONTROL ET
    oto_vars.o_state =  _2_de_bekleyen_istasyon_1_e_goturmeyi_bekle;
    break;
  case  _2_de_bekleyen_istasyon_1_e_goturmeyi_bekle:
    if (istasyona_git_kontrolu())
    {
      oto_vars.o_state =  _2_de_bekleyen_istasyon_1_de_asagi_in;
    }

    break;
  case  _2_de_bekleyen_istasyon_1_de_asagi_in:
    oto_asagi_in_basla();
    oto_vars.o_state =  _2_de_bekleyen_istasyon_1_de_asagi_inmeyi_bekle;
    break;
  case  _2_de_bekleyen_istasyon_1_de_asagi_inmeyi_bekle:
    if (Oto_Asagi_in_kontrolu()) 
    {
      oto_vars.o_state =  start_veya_zaman_bekle;
    }

    break;
  case zaman_bekle:
    otomatikCevrimZamankontrol();
    break;
  default:
    break;
  }
  
}
uint8_t is_bitirme_kontrolu() // true donerse is bitmistir.
{
  uint8_t result = false;
  if(oto_vars.is_bitirme_aktif == true && istasyon_no > 8 && oto_vars.o_state == zaman_bekle) //istasyon no 8 den kucukse kesin is vardir. ve zaman bekle sadece tambur kendi yerine kondugu zaman aktif olur . 3 kontrol birden var. 
  {
    result = true;
    for(int i =0; i<9; i++)
    {
      if(tambur_zamanlama[i].IsRunning == true)
      {
       result = false;
       break;
      }
    }
  }
return result;


}
void otomatik_cevrim_degiskenleri_sifirla()
{
 oto_vars.aktif_tambur_no = 0 ; 
 oto_vars.bekleyen_tambur_no = 0 ; 
 oto_vars.kontrol_durumu = 0 ; 
 oto_vars.islem_basladi = 0 ; 
 oto_vars.is_bitirme_aktif= 0 ; 
 oto_vars.o_state = istasyona_git;
 otomatik_cevrim_start = 0 ;
 start_flag = 0;
 stop_flag =0 ;
 tambur_i_b_state = i_b_istasyona_git;
 tambur_kontrol_no = 9;
 t_state = Ilk_Kontrol;
 for(int i = 0 ; i< 9 ;i++)
 {
 tambur_zamanlama[i].isPaused = 0; 
 tambur_zamanlama[i].IsRunning = 0;  
 tambur_zamanlama[i].Time = 0;
 tambur_zamanlama[i].Timeout = 0;
 }
}

void hata_kodu_gonder(uint8_t err_code)
{
 hata_kodu_bilgisi_gonder = RETRY_SEND;
 hata_kodu = err_code ; 
}    
void tambur_tum_degerleri_gonder(uint8_t tambur_no) 
{
  tambur_tum_deger_gonder = 1;
  tum_degerleri_gonderilecek_tambur_no = tambur_no;
}   
void tambur_aktif_deger_gonder(uint8_t tambur_no) //// ZAMAN DURUMU
{
  tambur_aktif_zaman_degeri_gonder = 1 ;
  aktif_degeri_gonderilecek_tambur_no = tambur_no ;
}    
void tambur_aktiflik_durumu_gonder(uint8_t tambur_no,uint8_t value)
{
   tambur_durumu_gonder = 1 ;
   durumu_gonderilecek_tambur_no = tambur_no ;

}    
void tambur_sayaci_basladi(uint8_t tambur)
{
 sayac_bilgisi_gonderilecek_tambur_no = tambur;
 tambur_sayaci_basladi_gonder = 1 ;
}  

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


