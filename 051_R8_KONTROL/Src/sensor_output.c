#include "sensor_output.h"


OUTPUT_sens Sol_Output               = {Surucu_Sol_PORT,Surucu_Sol_PIN,450,0,0,0};
OUTPUT_sens Sag_Output               = {Surucu_Sag_PORT,Surucu_Sag_PIN,450,0,0,0};
OUTPUT_sens tamburDondur_Output      = {Tambur_Dondurme_PORT,Tambur_Dondurme_PIN,40,0,0,0};
OUTPUT_sens V_Asagi_Output           = {Vinc_Asagi_PORT,Vinc_Asagi_PIN,450,0,0,0};
OUTPUT_sens V_Yukari_Output          = {Vinc_Yukari_PORT,Vinc_Yukari_PIN,450,0,0,0};


void output_control(OUTPUT_sens *sens)
{
  if(sens->output_active)
  {
   if(++sens->time_counter >= sens->delay)
   {
    HAL_GPIO_WritePin(sens->port,sens->pin_no,GPIO_PIN_SET);  
   }
   if(sens->time_counter >= (sens->debounce + sens->delay))
   {
      HAL_GPIO_WritePin(sens->port,sens->pin_no,GPIO_PIN_RESET); 
      sens->time_counter = 0 ; 
      sens->output_active = 0;     
   } 
  }
}

void chkOutput()
{
 output_control(&Sol_Output);
 output_control(&Sag_Output);
 output_control(&tamburDondur_Output);
 output_control(&V_Asagi_Output);
 output_control(&V_Yukari_Output);
}

void ResetAndStartSens(OUTPUT_sens *sens) // time counter i sifirlar cikisi aktiflestirir
{
  sens->time_counter = 0; 
  sens->output_active = 1;   
}
void ResetAndStopSens(OUTPUT_sens *sens) // time counter i sifirlar cikisi aktiflestirir
{
  sens->time_counter = 0; 
  sens->output_active = 0;   
}
