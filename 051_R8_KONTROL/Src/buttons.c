#include <stdint.h>
#include "buttons.h"

KEYPAD g_bt_sag_sens        = {0, 0, 0, 0, 0, 0, 10 , SS_VINC_SAG_PIN,SS_VINC_SAG_PORT};
KEYPAD g_bt_orta_sens       = {0, 0, 0, 0, 0, 0, 10 , SS_VINC_ORTA_PIN,SS_VINC_ORTA_PORT};
KEYPAD g_bt_sol_sens        = {0, 0, 0, 0, 0, 0, 10 , SS_VINC_SOL_PIN,SS_VINC_SOL_PORT};
KEYPAD g_bt_tanbur_asagi    = {0, 0, 0, 0, 0, 0, 10 , SS_VINC_ASAGI_PIN,SS_VINC_ASAGI_PORT};
KEYPAD g_bt_tanbur_yukari   = {0, 0, 0, 0, 0, 0, 10 , SS_VINC_YUKARI_PIN,SS_VINC_YUKARI_PORT};
KEYPAD g_bt_acil_stop       = {0, 0, 0, 0, 0, 0, 10 , ACIL_STOP_PIN,ACIL_STOP_PORT};
KEYPAD g_bt_reset           = {0, 0, 0, 0, 0, 0, 20 , RESET_PIN,RESET_PORT};
KEYPAD g_bt_home_switch     = {0, 0, 0, 0, 0, 0, 20 , HOME_SWITCH_PIN,HOME_SWITCH_PORT};
KEYPAD g_bt_end_switch      = {0, 0, 0, 0, 0, 0, 20 , END_SWITCH_PIN,END_SWITCH_PORT};

void checkKeypad(KEYPAD *pButton)
{
  /*
  * Bu k�s�mda 1 ms'de bir button kontrol� yap�l�r.Butona bas�lmaz ise gerekli I/O
  * devrede pull-up direnci �zerinden lojik 1 de�erini g�r�r.Butona bas�l�r ise 
  * lojik s�f�r de�erini al�r.�lk durumda pButton->pressed 0 d�r ve butona bas�l�
  * olmad��� IO lojik 1 durumundad�r.Butona bas�l�r ise lojik 0 olur.
  * pButton->pressed ve inp ikisi de 0 oldu�unda dbc counter artar 20ms sonra 
  * pButton->pressed ex-or 'lan�r ve lojik 1 olur. Bu durumda pushFlag durumu 1 olur.
  * elimizi butona basmay� b�rakt���m�zda IO pull-up direnci �zerinden lojik 1 olur.
  * Bu s�rada bir �nceki ad�mda lojik 1 olan pButton->pressed ile ayn� de�erde olmu� olur.
  * 20 ms'ne s�resince ikisi birbirine e�it olur ve pButton->pressed ex-or 'lan�r ve lojik 0
  * olur.Bu durumda pushFlag=0 ve pullFlag=1 olur yani elimizi �ekti�imiz belli olur.
  * pButton->pressed ex-or'ln�p 1 olursa pushFlag=1 , pullFlag=0, 
  * pButton->pressed ex-or'ln�p 0 olursa pushFlag=0 , pullFlag=1 olur
  */
  
    uint8_t inp;
    inp =  HAL_GPIO_ReadPin((pButton->bPort),(pButton->bPin));
    //inp =  GPIO_ReadInputDataBit((pButton->bPort), (pButton->bPin));
//------------------------------------------------------------------------------
// Bu k�s�m buton durumunda lojik de�i�im olursa DBC_MAX (300) kadar �rnek sonra
// yani 1ms*300=300ms sonra duruma g�re pushFlag'i veya pullFlag'i set eder.
//------------------------------------------------------------------------------
  if (inp == pButton->pressed) //
  {
    if (++pButton->dbc >= pButton->DBC_MAX) 
    {
      pButton->dbc = 0;
      pButton->pressed ^= 1;
      
      if (pButton->pressed)
      {
        pButton->pushFlag = 1;
        pButton->pullFlag = 0;
      }
      else
      {
        pButton->pushFlag = 0;
        pButton->pullFlag = 1;
      }     
    }  
  }
  else
    pButton->dbc = 0;
  
//------------------------------------------------------------------------------
// Bu k�s�m butona bas�lma durumu LPC_MAX(3000) s�resini 1ms*3000=3sn yi a�arsa
// ilgili butonun LongPress bayra��n� set eder.  
//------------------------------------------------------------------------------  
  if(pButton->pressed && (!inp))
  {
    if(++pButton->LPressCntr >= LPC_MAX)
    {
      pButton->LPressCntr = 0;
      pButton->LongPress = 1;
    }
  }
  else
  {
    //pButton->LongPress = 0;
    pButton->LPressCntr = 0;
  }
}

void ChkButtons(void)
{
  
  checkKeypad(&g_bt_sag_sens);
  checkKeypad(&g_bt_orta_sens);
  checkKeypad(&g_bt_sol_sens);
  checkKeypad(&g_bt_tanbur_asagi);
  checkKeypad(&g_bt_tanbur_yukari);
  checkKeypad(&g_bt_acil_stop); 
  checkKeypad(&g_bt_home_switch);
  checkKeypad(&g_bt_end_switch );
  checkKeypad(&g_bt_reset);
  
}

void buttonClearallFlag(void) 
{
  g_bt_sag_sens.pullFlag =0;
  g_bt_sag_sens.pushFlag = 0;
  g_bt_sag_sens.LongPress = 0;

  g_bt_sol_sens.pullFlag =0;
  g_bt_sol_sens.pushFlag = 0;
  g_bt_sol_sens.LongPress = 0;

  g_bt_tanbur_asagi.pullFlag =0;
  g_bt_tanbur_asagi.pushFlag = 0;
  g_bt_tanbur_asagi.LongPress = 0;
    
  g_bt_tanbur_yukari.pullFlag =0;
  g_bt_tanbur_yukari.pushFlag = 0;
  g_bt_tanbur_yukari.LongPress = 0;
    
  g_bt_orta_sens.pullFlag =0;
  g_bt_orta_sens.pushFlag = 0;
  g_bt_orta_sens.LongPress = 0;
}
void clear_flag(KEYPAD *btn)
{
 btn->pullFlag = 0  ;
 btn->pushFlag = 0 ;
 btn->LongPress = 0 ;
}