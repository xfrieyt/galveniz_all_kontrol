#ifndef __BUTTONS_H
#define __BUTTONS_H

#include "stm32f0xx_hal.h"
#include "kontrol_input.h"


//#define DBC_MAX     100    // debounce sures�
#define LPC_MAX     2000   // uzun basma suresi

  //---------------------------------------------------------------------------------------//
  //                               ENUM-STRUCT TANIMLAMALARI                               //
  //---------------------------------------------------------------------------------------//
  
  typedef struct 
  {
    unsigned  pressed   :1;
    unsigned  pushFlag  :1;
    unsigned  pullFlag  :1;
    
    unsigned  LongPress :1;
    unsigned  LPressCntr;  
    
    uint8_t   dbc;
    uint16_t  DBC_MAX;
    uint16_t  bPin;
    GPIO_TypeDef* bPort;
  } KEYPAD;
  //---------------------------------------------------------------------------------------//
  //                               FONKS�YONLAR                                            //
  //---------------------------------------------------------------------------------------//
  void checkKeypad(KEYPAD *pButton);
  void ChkButtons(void);
  void buttonClearallFlag(void);
  void clear_flag(KEYPAD *btn);

extern KEYPAD g_bt_sag_sens      ; 
extern KEYPAD g_bt_orta_sens     ;
extern KEYPAD g_bt_sol_sens      ;
extern KEYPAD g_bt_tanbur_asagi  ;
extern KEYPAD g_bt_tanbur_yukari ;
extern KEYPAD g_bt_acil_stop     ;
extern KEYPAD g_bt_home_switch   ;
extern KEYPAD g_bt_end_switch    ;
extern KEYPAD g_bt_reset         ;
#endif /* __BUTTONS_H */