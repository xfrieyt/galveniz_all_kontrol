#ifndef __OTOMAT_H
#define __OTOMAT_H

#include "stdint.h"
#include "string.h"
#include "radio_spi.h"
#include "spirit1_appli.h"
#include "tim.h"
#include "commands.h"

#define SYN1 0x23
#define SYN2 0x24
#define SYN3 0x25

#define BUF_SIZE 6

#define SYN1_ADRES 0
#define SYN2_ADRES 1 
#define FUNCTION_ID_ADRES 2 
#define DATA_1_ADRES 3 
#define DATA_2_ADRES 4
#define SYN3_ADRES 5


#define z_BUF_SIZE 13

#define z_SYN1_ADRES 0
#define z_SYN2_ADRES 1 
#define z_FUNCTION_ID_ADRES 2 
#define z_TAMBUR_NO_ADRES   3 
#define z_IST_7_ADRES       4
#define z_IST_6_ADRES       5
#define z_IST_5_ADRES       6
#define z_IST_4_ADRES       7
#define z_IST_3_ADRES       8
#define z_IST_2_ADRES       9
#define z_IST_OWN          10
#define z_FLAG_ADRES       11
#define z_SYN3_ADRES       12


enum Otomat_State
{
  SYN1_e,
  SYN2_e,
  Function_ID_e,
  Data_1_e,
  Data_2_e,
  SYN3_e  
};

typedef struct 
{
  uint8_t	        IsRunning;		// timer calisma durumu
  volatile uint8_t	Timeout;		// timer tamamlanma durumu
  uint32_t		Time;			// aktif sure
  uint8_t               isPaused;
  //uint32_t 		SetTime;                // set edilecek deger
  uint8_t i_7_time;                             //timer set degerleri  
  uint8_t i_6_time;                             //timer set degerleri
  uint8_t i_5_time;                             //timer set degerleri
  uint8_t i_4_time;                             //timer set degerleri
  uint8_t i_3_time;                             //timer set degerleri
  uint8_t i_2_time;                             //timer set degerleri
  uint8_t first_time;
  union
  {
   uint8_t flag;
   struct
   {
     uint8_t i_7_f: 1 ;
     uint8_t i_6_f: 1 ;
     uint8_t i_5_f: 1 ;
     uint8_t i_4_f: 1 ;
     uint8_t i_3_f: 1 ;
     uint8_t i_2_f: 1 ;
     uint8_t aktiflik :1;
     uint8_t reserve :1;
     
   }bits;
  }t_flag;
}tambur_time;

extern uint8_t TX_usage_buffer[30];
extern uint8_t TX_usage_buffer_sayisi ;
extern RadioDriver_t *pRadioDriver_tx;
extern RadioDriver_t *pRadioDriver_rx;
void Kablosuz_Veri_Gonder(uint8_t func_id,uint8_t data_1,uint8_t data_2);
void Kablosuz_Zaman_Verisi_Aktar(tambur_time t_var,uint8_t tambur_no);
void istasyon_no_kontrol();











#endif