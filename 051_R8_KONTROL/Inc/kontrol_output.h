#ifndef __KONTROL_OUTPUT_H
#define __KONTROL_OUTPUT_H

#include "stm32f0xx_hal.h"

#include "buttons.h"

//#define Surucu_Sag_PIN        GPIO_PIN_5
//#define Surucu_Sol_PIN        GPIO_PIN_4
//#define Vinc_Asagi_PIN        GPIO_PIN_3
//#define Vinc_Yukari_PIN       GPIO_PIN_2
//#define Tambur_Dondurme_PIN   GPIO_PIN_1
//#define Surucu_Yavas_1_PIN    GPIO_PIN_0
//#define Surucu_Yavas_0_PIN    GPIO_PIN_3
//#define KIRMIZI_SINYAL_PIN    GPIO_PIN_2
//
//
//#define Surucu_Sag_PORT       GPIOF
//#define Surucu_Sol_PORT       GPIOF
//#define Vinc_Asagi_PORT       GPIOA
//#define Vinc_Yukari_PORT      GPIOA
//#define Tambur_Dondurme_PORT  GPIOA
//#define Surucu_Yavas_1_PORT   GPIOA
//#define Surucu_Yavas_0_PORT   GPIOC
//#define KIRMIZI_SINYAL_PORT   GPIOC
//

#define Surucu_Sag_PIN        GPIO_PIN_0
#define Surucu_Sol_PIN        GPIO_PIN_1
#define Vinc_Asagi_PIN        GPIO_PIN_0    // DIYOT
#define Vinc_Yukari_PIN       GPIO_PIN_1    // DIYOT
#define Tambur_Dondurme_PIN   GPIO_PIN_2    // DIYOT
#define Surucu_Yavas_1_PIN    GPIO_PIN_3
#define Surucu_Yavas_0_PIN    GPIO_PIN_0
#define KIRMIZI_SINYAL_PIN    GPIO_PIN_1
#define YESIL_SINYAL_PIN      GPIO_PIN_2   //C2
#define TURUNCU_SINYAL_PIN    GPIO_PIN_3


#define Surucu_Sag_PORT       GPIOF
#define Surucu_Sol_PORT       GPIOF
#define Vinc_Asagi_PORT       GPIOC
#define Vinc_Yukari_PORT      GPIOC
#define Tambur_Dondurme_PORT  GPIOC
#define Surucu_Yavas_1_PORT   GPIOC
#define Surucu_Yavas_0_PORT   GPIOA

#define KIRMIZI_SINYAL_PORT   GPIOA
#define YESIL_SINYAL_PORT     GPIOA
#define TURUNCU_SINYAL_PORT   GPIOA

//1 f0
//2 f1
//7 a0
//8 a1
//9 a2
//10 a3
//11 f4
//12 f5


#define Uyari_1_ON           HAL_GPIO_WritePin(KIRMIZI_SINYAL_PORT,KIRMIZI_SINYAL_PIN,GPIO_PIN_SET)
#define Uyari_1_OFF           HAL_GPIO_WritePin(KIRMIZI_SINYAL_PORT,KIRMIZI_SINYAL_PIN,GPIO_PIN_RESET)

#define Uyari_2_ON           HAL_GPIO_WritePin(YESIL_SINYAL_PORT,YESIL_SINYAL_PIN,GPIO_PIN_SET)
#define Uyari_2_OFF           HAL_GPIO_WritePin(YESIL_SINYAL_PORT,YESIL_SINYAL_PIN,GPIO_PIN_RESET)

#define Uyari_3_ON           HAL_GPIO_WritePin(TURUNCU_SINYAL_PORT,TURUNCU_SINYAL_PIN,GPIO_PIN_SET)
#define Uyari_3_OFF           HAL_GPIO_WritePin(TURUNCU_SINYAL_PORT,TURUNCU_SINYAL_PIN,GPIO_PIN_RESET)


#define Surucu_Sol_ON        HAL_GPIO_WritePin(Surucu_Sol_PORT,Surucu_Sol_PIN,GPIO_PIN_SET)
#define Surucu_Sol_OFF       HAL_GPIO_WritePin(Surucu_Sol_PORT,Surucu_Sol_PIN,GPIO_PIN_RESET)
                             
#define Surucu_Sag_ON        HAL_GPIO_WritePin(Surucu_Sag_PORT,Surucu_Sag_PIN,GPIO_PIN_SET)
#define Surucu_Sag_OFF       HAL_GPIO_WritePin(Surucu_Sag_PORT,Surucu_Sag_PIN,GPIO_PIN_RESET)
                             
#define Surucu_Sinyal_1_ON   HAL_GPIO_WritePin(Surucu_Yavas_1_PORT,Surucu_Yavas_1_PIN,GPIO_PIN_SET)
#define Surucu_Sinyal_1_OFF  HAL_GPIO_WritePin(Surucu_Yavas_1_PORT,Surucu_Yavas_1_PIN,GPIO_PIN_RESET)
                             
#define Surucu_Sinyal_2_ON   HAL_GPIO_WritePin(Surucu_Yavas_0_PORT,Surucu_Yavas_0_PIN,GPIO_PIN_SET)
#define Surucu_Sinyal_2_OFF  HAL_GPIO_WritePin(Surucu_Yavas_0_PORT,Surucu_Yavas_0_PIN,GPIO_PIN_RESET)
                             
#define Vinc_Asagi_ON        HAL_GPIO_WritePin(Vinc_Asagi_PORT,Vinc_Asagi_PIN,GPIO_PIN_SET)
#define Vinc_Asagi_OFF       HAL_GPIO_WritePin(Vinc_Asagi_PORT,Vinc_Asagi_PIN,GPIO_PIN_RESET)
                             
#define Vinc_Yukari_ON       HAL_GPIO_WritePin(Vinc_Yukari_PORT,Vinc_Yukari_PIN,GPIO_PIN_SET)
#define Vinc_Yukari_OFF      HAL_GPIO_WritePin(Vinc_Yukari_PORT,Vinc_Yukari_PIN,GPIO_PIN_RESET)

#define Tambur_Dondurme_ON   HAL_GPIO_WritePin(Tambur_Dondurme_PORT,Tambur_Dondurme_PIN,GPIO_PIN_SET)
#define Tambur_Dondurme_OFF  HAL_GPIO_WritePin(Tambur_Dondurme_PORT,Tambur_Dondurme_PIN,GPIO_PIN_RESET)


typedef enum
{
 Vinc_Bilgisi_yok,
 Vinc_Asagida,
 Vinc_Belirsiz,
 Vinc_Yukarida,

}Vinc_Durumu_t;

typedef union 
{
uint8_t status;

struct
{
  
 uint8_t Surucu_Sol      : 1;
 uint8_t Surucu_Sag      : 1; 
 uint8_t Surucu_Yavas_1  : 1; 
 uint8_t Surucu_Yavas_0  : 1; 
 uint8_t Vinc_Asagi      : 1; 
 uint8_t Vinc_Yukari     : 1; 
 uint8_t Tambur_Dondurme : 1; 
 uint8_t KIRMIZI_SINYAL  : 1; 
 
}bits;


}Output_Union;


void output_durumu_guncelle();
void sola_git();
void saga_git();
void Yatay_Stop();
void Yukari_Cik();
void Asagi_In();
void Dikey_stop();
void Tambur_Dondur();
void Tambur_Dur();



#endif