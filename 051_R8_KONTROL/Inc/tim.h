#ifndef __TIM_H
#define __TIM_H
//---------------------------------------------------------------------------------------//
//                               DEFININGS                                               //
//---------------------------------------------------------------------------------------//
#define TIMERS_SIZE 15



typedef enum 
{ 
 tanbur_yukari_kontrol,
 tanbur_asagi_kontrol,
 Kablosuz_Gonderme_Gecikmesi,
 ise_baslama_tambur_gecikmesi,
 orta_sensor_kontrol_gecikmesi,
 otomatik_cevrim_basladi_gecikmesi,
}Soft_Timer;

extern volatile uint32_t TIMERS[TIMERS_SIZE];


//---------------------------------------------------------------------------------------//
//                               EXTERN EDİLMİŞ DEĞİŞKENLER                              //
//---------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------//
//                               FONKSİYONLAR                                            //
//---------------------------------------------------------------------------------------//
void DEC_TIMERS(void);
#endif 
