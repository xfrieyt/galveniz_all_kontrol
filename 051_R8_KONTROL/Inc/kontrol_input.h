#ifndef __KONTROL_INPUT_H
#define __KONTROL_INPUT_H

#include "stm32f0xx_hal.h"
                                     
#define SS_VINC_SAG_PIN             GPIO_PIN_9        // INPUT_1 
#define SS_VINC_SOL_PIN             GPIO_PIN_8        // INPUT_2 
#define SS_VINC_ORTA_PIN            GPIO_PIN_9        // INPUT_3 
#define SS_VINC_ASAGI_PIN           GPIO_PIN_10       // INPUT_4 
#define SS_VINC_YUKARI_PIN          GPIO_PIN_11       // INPUT_5 
#define SS_TANBUR_VAR_PIN           GPIO_PIN_12       // INPUT_6 
#define ACIL_STOP_PIN               GPIO_PIN_6        // INPUT_7 
#define RESET_PIN                   GPIO_PIN_7        // INPUT_8 
#define END_SWITCH_PIN              GPIO_PIN_15       // INPUT_9 
#define HOME_SWITCH_PIN             GPIO_PIN_10       // INPUT_10
#define JY_YUKARI_PIN               GPIO_PIN_11       // INPUT_11
#define JY_ASAGI_PIN                GPIO_PIN_12       // INPUT_12
#define JY_SAG_PIN                  GPIO_PIN_2        // INPUT_13
#define JY_SOL_PIN                  GPIO_PIN_3        // INPUT_14
#define INPUT14_PIN                 GPIO_PIN_4        // INPUT_15
#define INPUT15_PIN                 GPIO_PIN_8        // INPUT_16


      
      
      
      

   


#define SS_VINC_SAG_PORT              GPIOC           // INPUT_1 
#define SS_VINC_SOL_PORT              GPIOA           // INPUT_2 
#define SS_VINC_ORTA_PORT             GPIOA           // INPUT_3 
#define SS_VINC_YUKARI_PORT           GPIOA           // INPUT_4 
#define SS_VINC_ASAGI_PORT            GPIOA           // INPUT_5 
#define SS_TANBUR_VAR_PORT            GPIOA           // INPUT_6 
#define ACIL_STOP_PORT                GPIOF           // INPUT_7 
#define RESET_PORT                    GPIOF           // INPUT_8 
#define END_SWITCH_PORT               GPIOA           // INPUT_9 
#define HOME_SWITCH_PORT              GPIOC           // INPUT_10
#define JY_YUKARI_PORT                GPIOC           // INPUT_11
#define JY_ASAGI_PORT                 GPIOC           // INPUT_12
#define JY_SAG_PORT                   GPIOD           // INPUT_13
#define JY_SOL_PORT                   GPIOB           // INPUT_14
#define INPUT14_PORT                  GPIOB           // INPUT_15
#define INPUT15_PORT                  GPIOB           // INPUT_16


              
              
                         
              


typedef union 
{
uint8_t status[2];

struct
{
  
 uint8_t ACIL_STOP              : 1;
 uint8_t JY_YUKARI              : 1; 
 uint8_t JY_ASAGI               : 1; 
 uint8_t JY_SAG                 : 1; 
 uint8_t JY_SOL                 : 1; 
 uint8_t SS_TANBUR_VAR          : 1; 
 uint8_t SS_VINC_SAG            : 1; 
 uint8_t SS_VINC_ORTA           : 1; 
 uint8_t SS_VINC_SOL            : 1;
 uint8_t SS_VINC_YUKARI         : 1; 
 uint8_t SS_VINC_ASAGI          : 1; 
 uint8_t HOME_SWITCH            : 1; 
 uint8_t END_SWITCH             : 1; 
 uint8_t VINC_YUKARI_END_SWITCH : 1; 
 uint8_t INPUT14                : 1; 
 uint8_t INPUT15                : 1; 
 
}bits;


}Input_Union;

uint8_t pull_up_input_read(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin);
uint8_t pull_down_input_read(GPIO_TypeDef* GPIOx,uint16_t GPIO_Pin);
void input_durumu_guncelle();


extern Input_Union input_Stat;


#endif
