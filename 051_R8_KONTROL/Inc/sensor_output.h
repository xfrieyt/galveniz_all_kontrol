#ifndef __SENSOR_OUTPUT_H__
#define __SENSOR_OUTPUT_H__

#include "main.h"
#include "stm32f0xx_hal.h"
#include "kontrol_output.h"

typedef struct 
{
 GPIO_TypeDef* port;
 uint16_t pin_no;
 uint16_t debounce;
 uint16_t delay;
 uint16_t time_counter;
 uint8_t output_active;
}OUTPUT_sens;


extern OUTPUT_sens Sol_Output       ;
extern OUTPUT_sens Sag_Output       ;
extern OUTPUT_sens tamburDondur_Output      ;
extern OUTPUT_sens V_Asagi_Output ;
extern OUTPUT_sens V_Yukari_Output;

void output_control(OUTPUT_sens *sens);
void chkOutput();
void ResetAndStartSens(OUTPUT_sens *sens);
void ResetAndStopSens(OUTPUT_sens *sens);
#endif