#ifndef __ANA_ALGORITMA_H
#define __ANA_ALGORITMA_H

#include "stdint.h"
#include "kontrol_output.h"
#include "buttons.h" 
#include "tim.h"
#include "Otomat.h"
#include "commands.h"
#include "sensor_output.h"

#define SAG_YONE 1
#define SOL_YONE 2
#define STOP     0

#define RETRY_SEND 1

#define asagi_in_gecikmesi       kontrol_timing[0]
#define asagi_inis_suresi        kontrol_timing[1]
#define asagida_ilk_kalma_suresi kontrol_timing[2] // ilk ini�ten sonra duracak ve bekleyecek . buradaki bekleme
#define asagida_son_kalma_suresi kontrol_timing[3] // en alta indikten sonra bekleyece�i s�re
#define yukari_cikma_suresi      kontrol_timing[4] //ilk etapta ne kadar s�re yukar� ��kacak
#define tanbur_dondurme_suresi   kontrol_timing[5] // tanbur dondurme suresi

#define const_asagi_in_gecikmesi       500
#define const_asagi_inis_suresi        5000
#define const_asagida_ilk_kalma_suresi 800 
#define const_asagida_son_kalma_suresi 100 // degisken k�s�m 
#define const_yukari_cikma_suresi      5000 
#define const_tanbur_dondurme_suresi   1500


#define false 0
#define true  1

#define tambur9devrede  0
#define tambur10devrede 1
#define tambur11devrede 2
#define tambur12devrede 3
#define tambur13devrede 4
#define tambur14devrede 5
#define tambur15devrede 6
#define tambur16devrede 7
#define tambur17devrede 8



typedef enum 
{
  Ilk_Kontrol,
  ise_baslama,
  RF_Sekansi,
  otomatik_cevrim,
  isi_bitirme
}Genel_loop;







void ana_kontrol();

void sinirAnahtarlariniKontrolEt();


void istasyona_gonder(uint8_t hedef_istasyon); //komut gonder yerine yazilacak
void oto_yukari_cik_basla();                   //komut gonder yerine yazilacak
void oto_asagi_in_basla();                     //komut gonder yerine yazilacak




uint8_t istasyona_git_kontrolu(); //if icinde kontrol et
uint8_t Oto_Asagi_in_kontrolu();           //if icinde kontrol et
uint8_t Oto_Yukari_Cik_kontrol();         //if icinde kontrol et


void Kablosuz_Veri_Gonderme_Islemi();
void Flash_Read_Timing();
void RF_Gelen_Paket_Cozumle(uint8_t* pckt);


void IseBaslamaIlkkontrol();
void iseBaslamaFonksiyonu();
void otomatikCevrimFonksiyonu();
void isBtirmeFonksiyonu();
uint8_t Tambur_Kontrol();
void ise_baslama_tambur_kontrol_dongusu(uint8_t tambur_no) ;
void otomatikCevrimZamankontrol();
void otomatikCevrimKontrolFonksiyonu();
void otomatik_cevrim_fonksiyonu();
void tambur_time_pause(uint8_t tambur_no);
void sinyal_durumu();
void tambur_time_SetRaw_time(uint8_t tambur_no,uint32_t time_val);
void tambur_time_SetIstasyon_time(uint8_t tambur_no,uint8_t istasyon_no );
uint8_t tambur_time_GetTimerStatus(uint8_t tambur_no);
void tambur_time_ResetTimer(uint8_t tambur_no);
uint8_t tambur_time_IsTimeUp(uint8_t tambur_no);
void tambur_time_pause(uint8_t tambur_no);
void tambur_time_ISR(void);
void tambur_time_SetRaw_time(uint8_t tambur_no,uint32_t time_val);
uint8_t tambur_istasyon_flag_al(uint8_t tambur_no,uint8_t istasyon_no);
void hata_kodu_gonder(uint8_t err_code);
void tambur_tum_degerleri_gonder(uint8_t tambur_no);
void tambur_aktif_deger_gonder(uint8_t tambur_no);
uint16_t Convert_Aktif_Zaman_to_Dakika(uint32_t millisecond);
void tambur_aktiflik_durumu_gonder(uint8_t tambur_no,uint8_t value);
void _45_Hz_Ayarla();
void _10_Hz_Ayarla();
void _70_Hz_Ayarla();
uint32_t Convert_Second_to_Millisecond(uint16_t second );
void tambur_sayaci_basladi(uint8_t tambur);
uint8_t is_bitirme_kontrolu();
void otomatik_cevrim_degiskenleri_sifirla();
uint32_t Convert_minute_to_millisecond(uint16_t minute);
uint16_t Convert_millisecond_to_minute(uint32_t millisecond) ;
#endif